<%@ page import="java.util.List" %>
<%@ page import="ru.itdrive.webChat.app.models.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>AllUsers</title>
</head>
<body>
<h>List of Users</h>
<% List<User> allUsers= (List<User>)request.getAttribute("userList");
    for (User currentUser: allUsers){%>
        <p><a href="/users?userId=<%=currentUser.getId()%>"> <%=currentUser.getName()%> </a>     <%=currentUser.getUserRole()%></p>
<%}%>
</body>
</html>
