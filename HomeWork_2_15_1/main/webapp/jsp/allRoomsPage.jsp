<%@ page import="ru.itdrive.webChat.app.models.Room" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>All rooms</title>
</head>
<body>
<h1>work with rooms</h1>
<p>You can create room</p>
<form action="/rooms?action=create" method="post">
    <input type="hidden" value="${_csrf_token}" name="_csrf_token">
    <input type="text" placeholder="Room name" name="roomName" >
    <input type="text" placeholder="Max count user" name="maxCountUser">
    <input type="submit" value="Create room">
</form>
<%
    List<Room> roomList = (List<Room>)request.getAttribute("roomList");
    if (roomList != null){
    for (Room currentRoom: roomList){%>
        <p><%=currentRoom.getId()%><a href="/rooms?roomId=<%=currentRoom.getId()%>"><%=currentRoom.getRoomName()%></a>
        <form action="/rooms?action=deleteRoom&roomId=<%=currentRoom.getId()%>">
            <input type="hidden" value="${_csrf_token}" name="_csrf_token">
            <input type="submit" value="Delete room">
        </form></p>
    <%}}%>
</body>
</html>
