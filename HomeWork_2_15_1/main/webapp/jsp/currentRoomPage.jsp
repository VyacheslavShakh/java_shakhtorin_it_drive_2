<%@ page import="ru.itdrive.webChat.app.models.Room" %>
<%@ page import="java.util.List" %>
<%@ page import="ru.itdrive.webChat.app.models.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Room info</title>
</head>
<body>
<p>Chat room</p>
<p>Name: ${currentRoom.roomName}</p>
<p>Maximum user count ${currentRoom.maxCountUsersInRoom}</p>
<p>You can add user in room</p>
<form action="/rooms?action=addUserInRoom&roomId=${currentRoom.id}" method="post">
    <input type="hidden" value="${_csrf_token}" name="_csrf_token">
    <input type="text" placeholder="User ID" name="userId">
    <input type="submit" value="Add User">
</form>
<p>Users already in room:</p>
<%  Room currentRoom = (Room) request.getAttribute("currentRoom");
    List<User> userList = (List<User>)request.getAttribute("usersInRoom");
    if (userList != null) {
        for (User user: userList){%>
        <p><a href="/users?userId=<%=user.getId()%>"><%=user.getName()%></a>
            <form action="/rooms?action=deleteUserFromRoom&roomId=<%=currentRoom.getId()%>&userId=<%=user.getId()%>" method="post">
                 <input type="hidden" value="${_csrf_token}" name="_csrf_token">
                 <input type="submit" value="Delete User">
            </form></p>
    <%}}%>
</body>
</html>
