<%@ page import="ru.itdrive.webChat.app.models.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
</head>
<body>
<% User user = (User) request.getAttribute("user"); %>
<h1>User information for user ID = <%=user.getId()%> </h1>
<p>User name = <%=user.getName()%></p>
<p>User password = <%=user.getPassword()%></p>
<form action="/users?action=delete&userId=<%=user.getId()%>" method="post">
    <input type="hidden" value="${_csrf_token}" name="_csrf_token">
    <input type="submit" value="Delete User">
</form>
</body>
</html>
