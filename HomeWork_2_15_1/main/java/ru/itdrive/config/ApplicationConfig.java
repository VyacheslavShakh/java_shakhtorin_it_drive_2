package ru.itdrive.config;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.util.ArrayList;

@Configuration
@ComponentScan(basePackages = "ru.itdrive")
@PropertySource(value = "classpath:application.properties")
public class ApplicationConfig {

    @Autowired
    Environment environment;

    @Bean
    public DataSource dataSource (){
        return new HikariDataSource(hikariConfig());
    }

    public HikariConfig hikariConfig(){
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setPassword(environment.getProperty("db.password"));
        hikariConfig.setUsername(environment.getProperty("db.user"));
        hikariConfig.setDriverClassName(environment.getProperty("db.driver.name"));
        hikariConfig.setJdbcUrl(environment.getProperty("db.url"));
        return hikariConfig;
    }

    @Bean
    public AppProperties appProperties(){
        AppProperties appProperties = AppProperties.
                builder().
                pathToFileFolderOnServer(environment.getProperty("PATHTOFILEFOLDER")).
                acceptedRolesForAdmin(new ArrayList<String>()).
                acceptedRolesForUser(new ArrayList<String>()).
                build();
        System.out.println(appProperties.getPathToFileFolderOnServer());
        String[] userRightArray = environment.getProperty("right.user").split(";");
        String[] adminRightArray = environment.getProperty("right.admin").split(";");
        for(String role: userRightArray){
            appProperties.getAcceptedRolesForUser().add(role);
        }
        for(String role: adminRightArray){
            appProperties.getAcceptedRolesForAdmin().add(role);
        }
        return appProperties;
    }
}
