package ru.itdrive.config;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppProperties {
    private String pathToFileFolderOnServer;
    private List<String> acceptedRolesForUser;
    private List<String> acceptedRolesForAdmin;
}
