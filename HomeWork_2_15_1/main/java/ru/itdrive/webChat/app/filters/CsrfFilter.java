package ru.itdrive.webChat.app.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.UUID;

import static ru.itdrive.webChat.app.filters.ResponseUtil.sendForbidden;


public class CsrfFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;

        if(request.getMethod().equals("POST")){
            String csrfSession = (String) request.getSession(false).getAttribute("_csrf_token");
            String csrfRequest = request.getParameter("_csrf_token");
            if(csrfRequest.equals(csrfSession)){
                filterChain.doFilter(request,response);
                return;
            } else {
                sendForbidden(request,response);
                return;
            }
        }

        if (request.getMethod().equals("GET")){
            HttpSession session = request.getSession(false);
            if (session==null){
                String csrf = UUID.randomUUID().toString();
                request.setAttribute("_csrf_token", csrf);
                session = request.getSession();
                session.setAttribute("_csrf_token", csrf);
            } else {
                request.setAttribute("_csrf_token",session.getAttribute("_csrf_token"));
            }
        }
        filterChain.doFilter(request,response);
    }
}
