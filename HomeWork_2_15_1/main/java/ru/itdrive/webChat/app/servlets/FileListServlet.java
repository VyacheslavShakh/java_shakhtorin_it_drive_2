package ru.itdrive.webChat.app.servlets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.itdrive.webChat.app.models.Authenication;
import ru.itdrive.webChat.app.models.FileInfo;
import ru.itdrive.webChat.app.models.User;
import ru.itdrive.webChat.app.services.FileListService;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@WebServlet("/filelist")
public class FileListServlet extends HttpServlet {

    @Autowired
    FileListService service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springConfig");
        service = applicationContext.getBean(FileListService.class);
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Authenication authenication = (Authenication) request.getSession(false).getAttribute("authentication");
        User user = authenication.getUser();
        List<FileInfo> fileInfoList = service.getFileListByUser(user.getId());
        Map<String, String> filePathsMap = new HashMap<>();
        String folderPath = service.getPatToServerFileFolders();
        for (FileInfo fileInfo: fileInfoList){
            String currentLink = fileInfo.getUuidFile();
            filePathsMap.put(currentLink, fileInfo.getFileName());
        }
        request.setAttribute("filePathsMap", filePathsMap);
        request.getRequestDispatcher("jsp/filelist.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
