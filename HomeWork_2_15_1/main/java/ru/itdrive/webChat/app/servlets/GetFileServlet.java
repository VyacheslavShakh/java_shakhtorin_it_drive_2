package ru.itdrive.webChat.app.servlets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.itdrive.webChat.app.models.Authenication;
import ru.itdrive.webChat.app.models.FileInfo;
import ru.itdrive.webChat.app.services.GetFileService;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;


@WebServlet("/file/*")
@MultipartConfig
public class GetFileServlet extends HttpServlet {

    @Autowired
    GetFileService service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springConfig");
        service = applicationContext.getBean(GetFileService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Authenication authenication = (Authenication) session.getAttribute("authentication");
        String[] requestUri = request.getRequestURI().split("/");
        System.out.println(request.getRequestURI());
        String fileUuid = requestUri[requestUri.length-1];
        System.out.println(fileUuid);
        FileInfo fileInfo = service.getFileByUuid(fileUuid);
        if(!fileInfo.getUserId().equals(authenication.getUser().getId())){
            response.setStatus(403);
            response.sendRedirect("html/errorPage.html");
        }
        String fullFileName = fileInfo.getUuidFile() + "." + fileInfo.getFileName().split("\\.")[fileInfo.getFileName().split("\\.").length-1];
        File currentFile = new File(service.getPathToFiles()+fullFileName);
        response.setContentType(fileInfo.getMimeType());
        response.setContentLength((int)(long)fileInfo.getFileSize());
        response.setHeader("Content-Disposition", "filename = " + fileInfo.getFileName() );
        Files.copy(currentFile.toPath(), response.getOutputStream());

    }
}
