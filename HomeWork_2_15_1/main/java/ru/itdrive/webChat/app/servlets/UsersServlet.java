package ru.itdrive.webChat.app.servlets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.itdrive.webChat.app.models.User;
import ru.itdrive.webChat.app.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/users")
public class UsersServlet extends HttpServlet {
    @Autowired
    UsersService service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springConfig");
        service = applicationContext.getBean(UsersService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userIdAsString = request.getParameter("userId");
        if (userIdAsString == null){
            request.setAttribute("userList", service.findAllUser());
            request.getRequestDispatcher("jsp/allUsersPage.jsp").forward(request,response);
        } else {
            Long userId = Long.valueOf(userIdAsString);
            Optional <User> optionalUser = service.findUserById(userId);
            if (optionalUser.isPresent()){
                User user = optionalUser.get();
                request.setAttribute("user", user);
                request.getRequestDispatcher("jsp/userPage.jsp").forward(request, response);
            } else {
                response.setStatus(404);
                request.getRequestDispatcher("html/errorPage.html").forward(request, response);
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if ((request.getParameter("action") != null) && request.getParameter("action").equals("delete")){
            service.deleteUser(Long.parseLong(request.getParameter("userId")));
        }
        response.sendRedirect("/profile");
    }
}
