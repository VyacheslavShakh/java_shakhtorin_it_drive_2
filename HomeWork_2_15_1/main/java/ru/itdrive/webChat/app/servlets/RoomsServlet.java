package ru.itdrive.webChat.app.servlets;

import org.springframework.context.ApplicationContext;
import ru.itdrive.webChat.app.models.Room;
import ru.itdrive.webChat.app.models.User;
import ru.itdrive.webChat.app.services.RoomService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/rooms")
public class RoomsServlet extends HttpServlet {

    RoomService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("roomId")==null){
            request.setAttribute("roomList", service.findAllRooms());
            request.getRequestDispatcher("jsp/allRoomsPage.jsp").forward(request, response);
        } else {
            Long roomId = Long.parseLong(request.getParameter("roomId"));
            Room currentRoom;
            if (service.findRoom(roomId).isPresent()){
                currentRoom = service.findRoom(roomId).get();
                List<User> usersInRoom = service.listUserInRoom(roomId);
                request.setAttribute("currentRoom", currentRoom);
                request.setAttribute("usersInRoom", usersInRoom);
                request.getRequestDispatcher("jsp/currentRoomPage.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("jsp/allRoomsPage.jsp").forward(request, response);
            }


        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String requestActionString = request.getParameter("action");
        switch (requestActionString){
            case ("create"):
                service.createRoom(request.getParameter("roomName"), Integer.parseInt(request.getParameter("maxCountUser")));
                response.sendRedirect("/rooms");
                break;
            case ("deleteRoom"):
                service.deleteRoom(Long.parseLong(request.getParameter("roomId")));
                response.sendRedirect("/rooms");
                break;
            case ("addUserInRoom"):
                service.addUserInRoom(Long.parseLong(request.getParameter("roomId")), Long.parseLong(request.getParameter("userId")));
                response.sendRedirect("/rooms?roomId="+request.getParameter("roomId"));
                break;
            case ("deleteUserFromRoom"):
                service.deleteUserFromRoom(Long.parseLong(request.getParameter("roomId")), Long.parseLong(request.getParameter("userId")));
                response.sendRedirect("/rooms?roomId="+request.getParameter("roomId"));
                break;
        }
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springConfig");
        service = applicationContext.getBean(RoomService.class);
    }
}
