package ru.itdrive.webChat.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.config.AppProperties;
import ru.itdrive.webChat.app.models.FileInfo;
import ru.itdrive.webChat.app.repositories.FileRepository;

@Component
public class GetFileServiceImpl implements GetFileService {

    @Autowired
    FileRepository fileRepository;

    @Autowired
    AppProperties appProperties;

    @Override
    public FileInfo getFileByUuid(String fileUuid){
        return fileRepository.findByUuid(fileUuid);
    }

    @Override
    public String getPathToFiles(){
        return appProperties.getPathToFileFolderOnServer();
    }

}
