package ru.itdrive.webChat.app.services;

import java.io.InputStream;

public interface UpLoadFileService {
    void upLoadFileOnServer(Long idUser, InputStream inputStream, String fileName, String fileType, Long fileSize);
}
