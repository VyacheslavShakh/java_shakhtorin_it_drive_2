package ru.itdrive.webChat.app.services;


import ru.itdrive.webChat.app.models.Room;
import ru.itdrive.webChat.app.models.User;

import java.util.List;
import java.util.Optional;

public interface RoomService {

    void createRoom(String name, int maxCountUser);

    void deleteRoom(Long roomId);

    Optional<Room> findRoom(Long roomId);

    List<Room> findAllRooms();

    void addUserInRoom (Long roomId, Long userId);

    void deleteUserFromRoom (Long roomId, Long userId);

    List<User> listUserInRoom (Long roomId);

}
