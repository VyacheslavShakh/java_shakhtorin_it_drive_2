package ru.itdrive.webChat.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.webChat.app.models.Room;
import ru.itdrive.webChat.app.models.User;
import ru.itdrive.webChat.app.models.UserRole;
import ru.itdrive.webChat.app.repositories.RoomRepository;
import ru.itdrive.webChat.app.repositories.UserInRoomRepository;
import ru.itdrive.webChat.app.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class RoomServiceImpl implements RoomService {

    @Autowired
    RoomRepository roomRepository;
    @Autowired
    UserInRoomRepository userInRoomRepository;
    @Autowired
    UserRepository userRepository;


    @Override
    public void createRoom(String name, int maxCountUser) {
        if (!roomRepository.findRoomByName(name).isPresent()) {
            roomRepository.save(Room.builder().
                    roomName(name).
                    maxCountUsersInRoom(maxCountUser).
                    build());
        }
    }

    @Override
    public void deleteRoom(Long roomId) {
        if (roomRepository.findById(roomId).isPresent()) {
            Room currentRoom = roomRepository.findById(roomId).get();
            currentRoom.setIsDeleted(true);
            roomRepository.update(currentRoom);
            userInRoomRepository.changeIsDeleteForRoom(roomId, true);
        }


    }

    @Override
    public Optional<Room> findRoom(Long roomId) {
        return roomRepository.findById(roomId);
    }

    @Override
    public List<Room> findAllRooms() {
        return roomRepository.findAll();
    }

    @Override
    public void addUserInRoom(Long roomId, Long userId) {
        if (roomRepository.findById(roomId).isPresent() && userRepository.findById(userId).isPresent()) {
            if(!userInRoomRepository.exist(roomId, userId)){
                userInRoomRepository.changeIsDeleteUserFromRoom(roomId, userId,false);
            } else {
                userInRoomRepository.addUserInRoom(roomId, userId);
            }
        }
    }

    @Override
    public List<User> listUserInRoom(Long roomId) {
        List <Long> usersId = userInRoomRepository.getUsersForRoom(roomId);
        List <User> users = new ArrayList<>();
        for (Long userId: usersId){
            if (userRepository.findById(userId).isPresent()){
                User currentUser = userRepository.findById(userId).get();
                if (!currentUser.getIsDeleted()){
                    if(userInRoomRepository.exist(roomId, userId)){
                        users.add(currentUser);
                    }
                }
            }
        }
        return users;
    }

    @Override
    public void deleteUserFromRoom(Long roomId, Long userId) {
        if (roomRepository.findById(roomId).isPresent() && userRepository.findById(userId).isPresent() && userInRoomRepository.exist(roomId,userId)) {
            userInRoomRepository.changeIsDeleteUserFromRoom(roomId, userId, true);
        }
    }
}
