package ru.itdrive.webChat.app.services;

import ru.itdrive.webChat.app.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersService {

    Optional<User> findUserById(Long id);

    void deleteUser(Long userId);

    List <User> findAllUser();

}
