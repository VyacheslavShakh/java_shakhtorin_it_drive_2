package ru.itdrive.webChat.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.config.AppProperties;
import ru.itdrive.webChat.app.models.FileInfo;
import ru.itdrive.webChat.app.repositories.FileRepository;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Component
public class UpLoadFileServiceImpl implements UpLoadFileService {

    @Autowired
    FileRepository fileRepository;
    @Autowired
    AppProperties appProperties;

    @Override
    public void upLoadFileOnServer(Long idUser, InputStream inputStream, String fileName, String fileType, Long fileSize){
        FileInfo fileInfo = FileInfo.builder().
                userId(idUser).
                fileName(fileName).
                uuidFile(UUID.randomUUID().toString()).
                mimeType(fileType).
                fileSize(fileSize).
                build();
        fileRepository.save(fileInfo);
        String[] wordsInFileName = fileName.split("\\.");

        try {
            Files.copy(inputStream, Paths.get(appProperties.getPathToFileFolderOnServer() + fileInfo.getUuidFile()+ "." + wordsInFileName[wordsInFileName.length-1]));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }
}
