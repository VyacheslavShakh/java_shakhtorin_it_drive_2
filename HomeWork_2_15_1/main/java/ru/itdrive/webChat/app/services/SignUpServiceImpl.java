package ru.itdrive.webChat.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.webChat.app.models.User;
import ru.itdrive.webChat.app.models.UserRole;
import ru.itdrive.webChat.app.repositories.UserRepository;

import java.util.Optional;

@Component
public class SignUpServiceImpl implements SignUpService {

    @Autowired
    UserRepository userRepository;

    @Override
    public Optional<User> userSignUp(String userName, String password) {
        Optional<User> optionalUser = userRepository.findUserByName(userName);
        User user;
        if (!optionalUser.isPresent()) {
            user = User.builder().name(userName).password(password).userRole(UserRole.valueOf("USER")).build();
            userRepository.save(user);
            return Optional.of(user);
        }
        return Optional.empty();
    }
}
