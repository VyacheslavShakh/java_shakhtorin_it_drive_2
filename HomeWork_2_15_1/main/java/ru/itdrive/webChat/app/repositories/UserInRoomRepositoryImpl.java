package ru.itdrive.webChat.app.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.webChat.app.models.Room;
import ru.itdrive.webChat.app.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserInRoomRepositoryImpl implements UserInRoomRepository {

    @Autowired
    DataSource dataSource;

    //language=SQL
    public final String SQL_ADD_USER_IN_ROOM = "insert into user_in_room (room_id, user_id, is_deleted) values (?,?, false)";
    //language=SQL
    public final String SQL_FIND_USER_BY_ROOM = "select * from user_in_room where room_id =?";
    //language=SQL
    public final String SQL_FIND_ROOM_BY_USER = "select * from user_in_room where user_id =?";
    //language=SQL
    public final String SQL_FIND_USER_IN_ROOM = "select * from user_in_room where room_id = ? and user_id =?";
    //language=SQL
    public final String SQL_CHANGE_IS_DELETED_USER_FROM_ROOM = "update user_in_room set is_deleted = ? where room_id = ? and user_id = ?";
    //language=SQL
    public final String SQL_CHANGE_IS_DELETED_ROOM = "update user_in_room set is_deleted = ? where room_id = ?";
    //language=SQL
    public final String SQL_CHANGE_IS_DELETED_USER = "update user_in_room set is_deleted = ? where user_id = ?";


    @Override
    public void changeIsDeleteUserFromRoom(Long roomId, Long userId, Boolean isDeleted) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CHANGE_IS_DELETED_USER_FROM_ROOM, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setBoolean(1,isDeleted);
            preparedStatement.setLong(2,roomId);
            preparedStatement.setLong(3,userId);
            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows!=1){
                throw new SQLException("Room-User update not executed");
            }

        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        } finally {
            if (preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public void changeIsDeleteForRoom(Long roomId, Boolean isDeleted) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CHANGE_IS_DELETED_ROOM, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setBoolean(1,isDeleted);
            preparedStatement.setLong(2,roomId);
            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows<1){
                throw new SQLException("Room-User update not executed");
            }

        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        } finally {
            if (preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public void changeIsDeleteForUser(Long userId, Boolean isDeleted) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CHANGE_IS_DELETED_USER, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setBoolean(1,isDeleted);
            preparedStatement.setLong(2,userId);
            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows<1){
                throw new SQLException("Room-User update not executed");
            }

        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        } finally {
            if (preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public void addUserInRoom(Long roomId, Long userId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(SQL_ADD_USER_IN_ROOM);
            preparedStatement.setLong(1,roomId);
            preparedStatement.setLong(2,userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        } finally {
            if (preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public List<Long> getUsersForRoom(Long roomId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Long> userList = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_USER_BY_ROOM);
            preparedStatement.setLong(1, roomId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                userList.add(resultSet.getLong("user_id"));
            }
            return userList;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public List<Long> getRoomsForUser(Long userId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Long> roomList = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ROOM_BY_USER);
            preparedStatement.setLong(1, userId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                roomList.add(resultSet.getLong("room_id"));
            }
            return roomList;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public boolean exist (Long roomId, Long userId){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_USER_IN_ROOM);
            preparedStatement.setLong(1, roomId);
            preparedStatement.setLong(2, userId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                return !resultSet.getBoolean("is_deleted");
            }
            return true;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }
}
