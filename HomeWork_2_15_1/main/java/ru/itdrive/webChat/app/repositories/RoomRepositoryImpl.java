package ru.itdrive.webChat.app.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.webChat.app.models.Room;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class RoomRepositoryImpl implements RoomRepository{

    @Autowired
    DataSource dataSource;

    //language=SQL
    public final String SQL_SAVE_ROOM = "insert into rooms_for_web (name, max_count_user) values (?,?)";
    //language=SQL
    public final String SQL_FIND_ROOM_BY_NAME = "select * from rooms_for_web where name = ?";
    //language=SQL
    public final String SQL_FIND_ROOM_BY_ID = "select * from rooms_for_web where id = ?";
    //language=SQL
    public final String SQL_FIND_ALL_ROOM = "select * from rooms_for_web";
    //language=SQL
    public final String SQL_UPDATE_ROOM = "update rooms_for_web set name = ?, max_count_user = ?, is_deleted = ? where id = ?";

    @Override
    public void save(Room entity) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SAVE_ROOM, statement.RETURN_GENERATED_KEYS);
            statement.setString(1, entity.getRoomName());
            statement.setInt(2,entity.getMaxCountUsersInRoom());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()){
                entity.setId(resultSet.getLong("id"));
            } else throw new SQLException("Room does not created");

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null){
                try {
                    resultSet.close();
                } catch (SQLException ignored) {}
            }
            if(statement != null){
                try {
                    statement.close();
                } catch (SQLException ignored) {}
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {}
            }
        }

    }

    @Override
    public void update(Room entity) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_ROOM, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1,entity.getRoomName());
            preparedStatement.setInt(2,entity.getMaxCountUsersInRoom());
            preparedStatement.setBoolean(3, entity.getIsDeleted());
            preparedStatement.setLong(4, entity.getId());
            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows!=1){
                throw new SQLException("Room update not executed");
            }

        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        } finally {
            if (preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public Optional<Room> findById(Long id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_ROOM_BY_ID);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()){
                return Optional.of(Room.builder()
                        .id(resultSet.getLong("id"))
                        .roomName(resultSet.getString("name"))
                        .maxCountUsersInRoom(resultSet.getInt("max_count_user"))
                        .isDeleted(resultSet.getBoolean("is_deleted"))
                        .build());
            }
            return Optional.empty();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null){
                try {
                    resultSet.close();
                } catch (SQLException ignored) {}
            }
            if(statement != null){
                try {
                    statement.close();
                } catch (SQLException ignored) {}
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {}
            }
        }
    }

    @Override
    public List<Room> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Room> roomList = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_ALL_ROOM);
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                roomList.add(Room.builder()
                        .id(resultSet.getLong("id"))
                        .roomName(resultSet.getString("name"))
                        .maxCountUsersInRoom(resultSet.getInt("max_count_user"))
                        .isDeleted(resultSet.getBoolean("is_deleted"))
                        .build());
            }
            return roomList;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null){
                try {
                    resultSet.close();
                } catch (SQLException ignored) {}
            }
            if(statement != null){
                try {
                    statement.close();
                } catch (SQLException ignored) {}
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {}
            }
        }
    }

    @Override
    public Optional<Room> findRoomByName(String name) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_ROOM_BY_NAME);
            statement.setString(1, name);
            resultSet = statement.executeQuery();
            if (resultSet.next()){
                return Optional.of(Room.builder()
                        .id(resultSet.getLong("id"))
                        .roomName(resultSet.getString("name"))
                        .maxCountUsersInRoom(resultSet.getInt("max_count_user"))
                        .isDeleted(resultSet.getBoolean("is_deleted"))
                        .build());
            }
            return Optional.empty();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null){
                try {
                    resultSet.close();
                } catch (SQLException ignored) {}
            }
            if(statement != null){
                try {
                    statement.close();
                } catch (SQLException ignored) {}
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {}
            }
        }
    }
}
