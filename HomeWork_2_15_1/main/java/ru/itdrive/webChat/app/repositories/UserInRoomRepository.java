package ru.itdrive.webChat.app.repositories;



import java.util.List;

public interface UserInRoomRepository {

    void changeIsDeleteUserFromRoom (Long roomId, Long userId, Boolean isDeleted);

    void addUserInRoom (Long roomId, Long userId);

    List<Long> getUsersForRoom (Long roomId);

    List<Long> getRoomsForUser (Long userId);

    void changeIsDeleteForRoom(Long roomId, Boolean isDeleted);

    void changeIsDeleteForUser(Long userId, Boolean isDeleted);

    boolean exist (Long roomId, Long userId);
}
