package ru.itdrive.webChat.app.repositories;

import ru.itdrive.webChat.app.models.Room;

import java.util.Optional;

public interface RoomRepository extends CrudRepository<Room> {

    Optional<Room> findRoomByName(String name);

}
