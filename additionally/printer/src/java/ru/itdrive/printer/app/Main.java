package ru.itdrive.printer.app;

import com.beust.jcommander.JCommander;
import ru.itdrive.printer.utils.Renderer;
import ru.itdrive.printer.utils.Arguments;


public class Main {

	public static void main(String[] args) {
		
	Arguments arguments = new Arguments();

	JCommander.newBuilder()
	          .addObject(arguments)
	          .build()
	          .parse(args);

        Renderer renderer = new Renderer();
        renderer.print("resources/image.bmp", arguments.white, arguments.black);
    }
}
