package ru.itdrive.printer.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import com.diogonunes.jcolor.*;


public class Renderer {
    public void print(String fileName, char white, char black){
        try {
            File imageFile = new File(fileName);
            BufferedImage image = ImageIO.read(imageFile);
            int height = image.getHeight();
            int width = image.getWidth();

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (image.getRGB(j, i) == Color.BLACK.getRGB()) {
                        System.out.print(black);
                    } else {
                        System.out.print(white);
                    }
                }
                System.out.println();
            }
        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

     public void print(String fileName, String white, String black){
        try {
            File imageFile = new File(fileName);
            BufferedImage image = ImageIO.read(imageFile);
            int height = image.getHeight();
            int width = image.getWidth();
            Attribute colorBlack = Attribute.BACK_COLOR (choseColor(black));
            Attribute colorWhite = Attribute.BACK_COLOR (choseColor(white));

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (image.getRGB(j, i) == Color.BLACK.getRGB()) {
                        System.out.print(Ansi.colorize("  ", colorBlack));
                    } else {
                        System.out.print(Ansi.colorize("  ", colorWhite));
                    }
                }
                System.out.println();
            }
        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    private int choseColor (String color) {
        switch (color){
            case "BLACK":
                return 0;
            case "RED":
                return 1;
            case "GREEN":
                return 2;
            case "YELLOW":
                return 3;
            case "BLUE":
                return 4;
            case "MAGENTA":
                return 5;
            case "CYAN":
                return 6;
            default:
                return 7;
        }

    }
}
