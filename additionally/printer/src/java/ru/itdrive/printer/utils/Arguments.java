package ru.itdrive.printer.utils;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.Parameter;

@Parameters(separators = "=")
public class Arguments {

	@Parameter(names = {"--black"})
	public String black;

	@Parameter(names = {"--white"})
	public String white;
}