package ru.itdrive.game.utils;

import java.util.Scanner;

import ru.itdrive.Pursuit.Catcher;

public class Game {

    boolean gameOver;
    MapService ms;
    int[][] map;
    int[] playerCoordinates;
    int[][] enemiesCoordinates;
    int[] goalCoordinates;


    public void startGame(int size, int wallsCount, int enemiesCount) {
        gameOver = false;
        ms = new MapService();
        map = ms.createMap(size, wallsCount, enemiesCount);
        ms.applyProperties();
        playerCoordinates = ms.getMap().getPlayerCoordinates();
        enemiesCoordinates = ms.getMap().getEnemiesCoordinates();
        goalCoordinates = ms.getMap().getGoalCoordinates();
        while (!gameOver) {
            ms.printMap();
            playerTurn();
            if ((playerCoordinates[0] == goalCoordinates[0])&&(playerCoordinates[1] == goalCoordinates[1])) {
                ms.printMap();
                System.out.println("Player WIN!!");
                break;
            }
            enemiesTurn();
            for (int i = 0; i < enemiesCoordinates.length; i++){
                if ((playerCoordinates[0] == enemiesCoordinates[i][0])&&(playerCoordinates[1] == enemiesCoordinates[i][1])){
                    System.out.println("Game over!!");
                    gameOver = true;
                    ms.printMap();
                    break;
                }
            }
        }

    }


    public boolean playerTurn() {
        Scanner scanner = new Scanner(System.in);
        String step = scanner.nextLine();
        int[] prevPlayerCoordinates = playerCoordinates;
        int[] stepTo = new int[2];
        if ((step.equals("w")) && (prevPlayerCoordinates[0] > 0)) {
            stepTo[0] = -1;
        } else if ((step.equals("a")) && (prevPlayerCoordinates[1] > 0)) {
            stepTo[1] = -1;
        } else if ((step.equals("s")) && (prevPlayerCoordinates[0] < map.length - 1)) {
            stepTo[0] = 1;
        } else if ((step.equals("d")) && (prevPlayerCoordinates[1] < map.length - 1)) {
            stepTo[1] = 1;
        }
        playerCoordinates = ms.step(3, prevPlayerCoordinates, stepTo);
        return playerCoordinates == prevPlayerCoordinates;
    }

    public void enemiesTurn() {
        Catcher catcher = new Catcher();
        int[] stepTo = catcher.findPath(ms.getMap().getMap(), enemiesCoordinates[0], playerCoordinates);

        for (int i = 0; i < enemiesCoordinates.length; i++) {
            enemiesCoordinates[i] = ms.step(2, enemiesCoordinates[i], catcher.findPath(ms.getMap().getMap(), enemiesCoordinates[i], playerCoordinates));
        }
    }


}
