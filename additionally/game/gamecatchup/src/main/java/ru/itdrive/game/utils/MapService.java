package ru.itdrive.game.utils;

import com.diogonunes.jcolor.Ansi;
import com.diogonunes.jcolor.Attribute;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class MapService {

    String[][] codes = {{"empty", "", ""}, {"wall", "", ""}, {"enemy", "", ""}, {"player", "", ""}, {"goal", "", ""}};
    Map map;

    public Map getMap() {
        return map;
    }

    public int[][] createMap(int size, int wallsCount, int enemiesCount) {
        this.map = new Map(size, wallsCount, enemiesCount);
        return this.map.getMap();
    }

    public void applyProperties() {
        try {
            File file = new File("src/main/resources/application-production.properties");
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                String properties = scanner.nextLine();
                String[] propertiesArray = properties.split(" ");
                String[] type = propertiesArray[0].split("\\.");
                for (int i = 0; i < codes.length; i++) {
                    if (type[0].equals(codes[i][0])) {
                        if (type[1].equals("char")) {
                            codes[i][1] = propertiesArray[2];
                        } else if (type[1].equals("color")) {
                            codes[i][2] = choseColor(propertiesArray[2]) + "";
                        }
                    }
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    public void printMap() {
        int[][] currentMap = map.getMap();
        Attribute color;
        for (int i = 0; i < currentMap.length; i++) {
            for (int j = 0; j < currentMap[i].length; j++) {
                color = Attribute.BACK_COLOR(Integer.parseInt(codes[currentMap[i][j]][2]));
                System.out.print(Ansi.colorize((codes[currentMap[i][j]][1]), color));
            }
            System.out.println();
        }

    }

    private int choseColor(String color) {
        if ("BLACK".equals(color)) {
            return 0;
        } else if ("RED".equals(color)) {
            return 1;
        } else if ("GREEN".equals(color)) {
            return 2;
        } else if ("YELLOW".equals(color)) {
            return 3;
        } else if ("BLUE".equals(color)) {
            return 4;
        } else if ("MAGENTA".equals(color)) {
            return 5;
        } else if ("CYAN".equals(color)) {
            return 6;
        }
        return 7;

    }

    public int [] step(int code, int[] from, int[] stepTo) {

        int[] to = new int[2];
        to[0] = from[0] + stepTo[0];
        to[1] = from[1] + stepTo[1];
        if ((map.getMap()[to[0]][to[1]] == 0)||(map.getMap()[to[0]][to[1]] == 3)||(map.getMap()[to[0]][to[1]] == 4)) {
            map.getMap()[from[0]][from[1]] = 0;
            map.getMap()[to[0]][to[1]] = code;
            return to;
        } else {
            return from;
        }

    }


}
