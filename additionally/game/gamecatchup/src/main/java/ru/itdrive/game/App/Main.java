package ru.itdrive.game.App;

import ru.itdrive.game.utils.Arguments;
import ru.itdrive.game.utils.Game;
import com.beust.jcommander.JCommander;

public class Main {
    public static void main(String[] args) {

        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        Game game = new Game();

        game.startGame(arguments.mapSize, arguments.wallsCount, arguments.enemiesCount);



    }
}
