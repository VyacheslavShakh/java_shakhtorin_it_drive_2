package ru.itdrive.game.utils;


import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Arguments {

    @Parameter(names = {"--MapSize"})
    public int mapSize;
    @Parameter(names = {"--WallsCount"})
    public int wallsCount;
    @Parameter(names = {"--EnemiesCount"})
    public int enemiesCount;


}
