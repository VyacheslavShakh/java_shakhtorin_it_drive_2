package ru.itdrive.game.utils;

import java.util.Random;

public class Map {
    Random random = new Random();
    int[][] map;
    int [] playerCoordinates;
    int [][] enemiesCoordinates;
    int [] goalCoordinates;


    public int[] getGoalCoordinates() { return goalCoordinates; }

    public int[] getPlayerCoordinates() {
        return playerCoordinates;
    }

    public int[][] getEnemiesCoordinates() {
        return enemiesCoordinates;
    }



    /*
    slot code
    0 - empty slot
    1 - wall
    2 - enemy
    3 - player
    4 - goal
     **/

    public Map(int size, int wallsCount, int enemiesCount){
        map = new int[size][size];
        playerCoordinates = new int[2];
        goalCoordinates = new int[2];
        enemiesCoordinates = new int [enemiesCount][2];
        for (int i = 0; i < map.length; i++){
            for (int j = 0; j < map.length; j++){
                map[j][i] = 0;
            }
        }
        randomCreation(1, wallsCount, size);
        randomCreation(2, enemiesCount, size);
        randomCreation(3, 1, size);
        randomCreation(4, 1, size);
    }

    private void randomCreation (int slotObjectsCode, int countObjects, int mapSize){
        for (int i = 0; i < countObjects; i++){
           int x = random.nextInt(mapSize);
           int y = random.nextInt(mapSize);
           if(map[y][x] == 0){
               map[y][x] = slotObjectsCode;
               if (slotObjectsCode == 3){
                   playerCoordinates[0] = y;
                   playerCoordinates[1] = x;
               } else if (slotObjectsCode == 2){
                   enemiesCoordinates[i][0] = y;
                   enemiesCoordinates[i][1] = x;
               } else if (slotObjectsCode == 4){
                   goalCoordinates[0] = y;
                   goalCoordinates[1] = x;
               }
           }else{
               i--;
           }
        }
    }

    public int[][] getMap() {
        return map;
    }
}
