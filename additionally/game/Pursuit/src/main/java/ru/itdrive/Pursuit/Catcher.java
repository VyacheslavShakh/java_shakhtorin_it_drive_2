package ru.itdrive.Pursuit;

public class Catcher {

    public int[] findPath(int[][] map, int[] start, int[] finish) {
        int[][] trackMap = new int[map.length][map.length];
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] != 0) {
                    if (map[i][j] != 3) {
                        trackMap[i][j] = 1000;
                    }
                }
            }
        }
        trackMap[start[0]][start[1]] = -1;

        int i = 0;
        int count;
        int laststep = 0;
        boolean end = true;
        while (end) {
            if (laststep < i - 1) {
                end = false;
            }
            if (i == 0) {
                count = -1;
            } else count = i;
            for (int y = 0; y < trackMap.length; y++) {
                for (int x = 0; x < trackMap[y].length; x++) {
                    if (count == trackMap[y][x]) {

                        laststep = i;
                        if ((finish[0] == y) && (finish[1] == x)) {
                            end = false;
                        }
                        if ((y > 0) && (trackMap[y - 1][x] == 0)) {
                            trackMap[y - 1][x] = i + 1;
                        }
                        if ((x < trackMap.length - 1) && (trackMap[y][x + 1] == 0)) {
                            trackMap[y][x + 1] = i + 1;
                        }
                        if ((y < trackMap.length - 1) && (trackMap[y + 1][x] == 0)) {
                            trackMap[y + 1][x] = i + 1;
                        }
                        if ((x > 0) && (trackMap[y][x - 1] == 0)) {
                            trackMap[y][x - 1] = i + 1;
                        }
                    }
                }
            }
            i++;
        }

        /*for (int y = 0; y < trackMap.length; y++) {
            for (int x = 0; x < trackMap.length; x++) {
                System.out.print("  " + ((trackMap[y][x] == 1000) ? ("X") : (trackMap[y][x])) + "  ");
            }
            System.out.println();
        }
        */

        int[] carrentStep = new int[2];

        int[] nextStep = new int[2];
        nextStep[0] = finish[0];
        nextStep[1] = finish[1];
        int min = trackMap[nextStep[0]][nextStep[1]];
        while (!(nextStep[0] == start[0]) | !(nextStep[1] == start[1])) {
            carrentStep[0] = nextStep[0];
            carrentStep[1] = nextStep[1];

            if ((carrentStep[0] > 0) && (trackMap[carrentStep[0] - 1][carrentStep[1]] < min)) {
                min = trackMap[carrentStep[0] - 1][carrentStep[1]];
                nextStep[0] = carrentStep[0] - 1;

            } else if ((carrentStep[1] > 0) && (trackMap[carrentStep[0]][carrentStep[1] - 1] < min)) {
                min = trackMap[carrentStep[0]][carrentStep[1] - 1];

                nextStep[1] = carrentStep[1] - 1;
            } else if ((carrentStep[0] < trackMap.length - 1) && (trackMap[carrentStep[0] + 1][carrentStep[1]] < min)) {
                min = trackMap[carrentStep[0] + 1][carrentStep[1]];

                nextStep[0] = carrentStep[0] + 1;
            } else if ((carrentStep[1] < trackMap.length - 1) && (trackMap[carrentStep[0]][carrentStep[1] + 1] < min)) {
                min = trackMap[carrentStep[0]][carrentStep[1] + 1];

                nextStep[1] = carrentStep[1] + 1;
            }
            if (min == 0) {
                break;
            }


        }
        int[] stepTo = new int[2];

        stepTo[0] = carrentStep[0] - start[0];
        stepTo[1] = carrentStep[1] - start[1];

        return stepTo;
    }

}
