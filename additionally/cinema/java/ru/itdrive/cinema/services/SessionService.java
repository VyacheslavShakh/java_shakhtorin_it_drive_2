package ru.itdrive.cinema.services;

import ru.itdrive.cinema.models.Session;
import ru.itdrive.cinema.models.SessionDTO;

import java.util.List;

public interface SessionService {

    void addSession(Session session);
    List<Session> getSessionList();
    Session getSession(Long id);
    SessionDTO getSessionDTO(Long id);
    List<SessionDTO> getAllSessionDTO();
}
