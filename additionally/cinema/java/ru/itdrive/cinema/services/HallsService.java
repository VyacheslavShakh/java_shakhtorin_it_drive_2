package ru.itdrive.cinema.services;

import ru.itdrive.cinema.models.Hall;

import java.util.List;

public interface HallsService {
    void addHall(Hall hall);
    Hall getHall(Long hallId);
    List<Hall> getAllHalls();

}
