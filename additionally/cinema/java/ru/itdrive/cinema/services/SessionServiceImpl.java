package ru.itdrive.cinema.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itdrive.cinema.models.Film;
import ru.itdrive.cinema.models.Hall;
import ru.itdrive.cinema.models.Session;
import ru.itdrive.cinema.models.SessionDTO;
import ru.itdrive.cinema.repositories.FilmRepository;
import ru.itdrive.cinema.repositories.HallRepository;
import ru.itdrive.cinema.repositories.SessionRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class SessionServiceImpl implements SessionService {

    @Autowired
    SessionRepository sessionRepository;
    @Autowired
    FilmRepository filmRepository;
    @Autowired
    HallRepository hallRepository;

    @Override
    public void addSession(Session session) {
        sessionRepository.save(session);
    }

    @Override
    public List<Session> getSessionList() {
        return sessionRepository.findAll();
    }

    @Override
    public Session getSession(Long id) {
        return sessionRepository.find(id);
    }

    @Override
    public SessionDTO getSessionDTO(Long id) {
        return null;
    }

    @Override
    public List<SessionDTO> getAllSessionDTO() {
        List<SessionDTO> sessionDTOList = new ArrayList<>();
        for (Session session:sessionRepository.findAll()){
            Film filmForSession = filmRepository.find(session.getFilmId());
            Hall hallForSession = hallRepository.find(session.getHallId());
            sessionDTOList.add(SessionDTO.builder()
                    .hallId(hallForSession.getId())
                    .filmId(filmForSession.getId())
                    .sessionTime(session.getSessionTime())
                    .ticketCost(session.getTicketCost())
                    .title(filmForSession.getTitle())
                    .releaseYear(filmForSession.getReleaseYear())
                    .ageRating(filmForSession.getAgeRating())
                    .description(filmForSession.getDescription())
                    .posterUrl(filmForSession.getPosterUrl())
                    .countPlace(hallForSession.getCountPlace())
                    .build());
        }
        return sessionDTOList;
    }
}
