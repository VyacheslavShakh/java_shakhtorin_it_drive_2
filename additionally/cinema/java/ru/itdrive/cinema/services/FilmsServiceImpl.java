package ru.itdrive.cinema.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itdrive.cinema.models.Film;
import ru.itdrive.cinema.repositories.FilmRepository;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Service
public class FilmsServiceImpl implements FilmsService {

    private static final String PATHTOPOSTER = "D://Java/serverfiles/";

    @Autowired
    FilmRepository filmRepository;

    @Override
    public void addFilm(Film film, InputStream posterStream) {
        String posterName = film.getPosterUrl();
        String posterUrl = UUID.randomUUID() + posterName.split("//.")[posterName.split("//.").length-1];
        film.setPosterUrl(posterUrl);
        try {
            Files.copy(posterStream, Paths.get(PATHTOPOSTER + posterUrl));
        } catch (IOException e) {
            e.printStackTrace();
        }
        filmRepository.save(film);
    }

    @Override
    public List<Film> getFilmList() {
        return filmRepository.findAll();

    }

    @Override
    public Film getFilm(Long id) {
        return null;
    }

    @Override
    public File returnPoster(String uuid) {
        return new File(PATHTOPOSTER + uuid);
    }
}
