package ru.itdrive.cinema.services;

import ru.itdrive.cinema.models.Film;

import java.io.File;
import java.io.InputStream;
import java.util.List;

public interface FilmsService {
    void addFilm(Film film, InputStream posterStream);
    List<Film> getFilmList();
    Film getFilm(Long id);
    File returnPoster(String uuid);
}
