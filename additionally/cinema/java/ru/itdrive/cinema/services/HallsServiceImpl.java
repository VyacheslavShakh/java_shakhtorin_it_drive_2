package ru.itdrive.cinema.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.itdrive.cinema.models.Hall;
import ru.itdrive.cinema.repositories.HallRepository;
import ru.itdrive.cinema.repositories.HallRepositoryImpl;

import javax.sql.DataSource;
import java.util.List;


@Service
public class HallsServiceImpl implements HallsService {

    @Autowired
    HallRepository hallRepository;


    @Override
    public void addHall(Hall hall) {
        hallRepository.save(hall);
    }

    @Override
    public Hall getHall(Long hallId) {
        return null;
    }

    @Override
    public List<Hall> getAllHalls() {
        return hallRepository.findAll();
    }


}
