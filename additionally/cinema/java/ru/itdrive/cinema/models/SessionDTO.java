package ru.itdrive.cinema.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SessionDTO {

    private Long sessionId;
    private Long hallId;
    private Long filmId;
    private String sessionTime;
    private Float ticketCost;
    private String title;
    private Integer releaseYear;
    private Integer ageRating;
    private String description;
    private String posterUrl;
    private Integer countPlace;

}
