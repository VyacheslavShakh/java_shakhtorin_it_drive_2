package ru.itdrive.cinema.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Session {

    private Long sessionId;
    private Long hallId;
    private Long filmId;
    private String sessionTime;
    private Float ticketCost;

}
