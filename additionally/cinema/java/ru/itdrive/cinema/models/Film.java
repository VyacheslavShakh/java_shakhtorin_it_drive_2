package ru.itdrive.cinema.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.OutputStream;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Film {

    private Long id;
    private String title;
    private Integer releaseYear;
    private Integer ageRating;
    private String description;
    private String posterUrl;


}
