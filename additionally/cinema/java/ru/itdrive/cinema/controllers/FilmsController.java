package ru.itdrive.cinema.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ru.itdrive.cinema.models.Film;
import ru.itdrive.cinema.services.FilmsService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.util.List;

@Controller
public class FilmsController {

    @Autowired
    FilmsService filmsService;

    @GetMapping("/admin/panel/films/poster/*")
    public void showPoster(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String[] posterURIArray = request.getRequestURI().split("/");
        String fileUuid = posterURIArray[posterURIArray.length-1];
        System.out.println(fileUuid);
        File posterFile = filmsService.returnPoster(fileUuid);
        Files.copy(posterFile.toPath(), response.getOutputStream());
    }

    @GetMapping("/admin/panel/films")
    public ModelAndView getFilms() throws IOException {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("films");
        List<Film> FilmList = filmsService.getFilmList();
        modelAndView.addObject("film_list", FilmList);
        return modelAndView;
    }

    @PostMapping("/admin/panel/films")
    public ModelAndView addFilm(@RequestParam(name = "poster")MultipartFile file, Film film) throws IOException, ServletException {
        film.setPosterUrl(file.getOriginalFilename());
        InputStream posterStream = file.getInputStream();
        filmsService.addFilm(film, posterStream);
        return new ModelAndView("redirect:/admin/panel/films");
    }

}
