package ru.itdrive.cinema.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.itdrive.cinema.models.Session;
import ru.itdrive.cinema.services.FilmsService;
import ru.itdrive.cinema.services.HallsService;
import ru.itdrive.cinema.services.SessionService;

@Controller
public class SessionController {

    @Autowired
    FilmsService filmsService;
    @Autowired
    HallsService hallsService;
    @Autowired
    SessionService sessionService;

    @GetMapping("/admin/panel/session")
    public ModelAndView getSessionPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("session");
        modelAndView.addObject("film_list", filmsService.getFilmList());
        modelAndView.addObject("hall_list", hallsService.getAllHalls());
        modelAndView.addObject("session_dto_list", sessionService.getAllSessionDTO());
        return modelAndView;
    }

    @PostMapping("/admin/panel/session")
    public ModelAndView createNewSession(Session session){
//        ModelAndView modelAndView = new ModelAndView("redirect:/admin/panel/session");
//        modelAndView.setViewName("session");
//        modelAndView.addObject("film_list", filmsService.getFilmList());
//        modelAndView.addObject("hall_list", hallsService.getAllHalls());
        sessionService.addSession(session);
        return new ModelAndView("redirect:/admin/panel/session");
    }


}
