package ru.itdrive.cinema.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.itdrive.cinema.models.Hall;
import ru.itdrive.cinema.services.HallsService;

import java.util.List;


@Controller
public class HallsController {

    @Autowired
    HallsService hallsService;

    @GetMapping("/admin/panel/halls")
    public ModelAndView getHallsPage(){
        ModelAndView modelAndView = new ModelAndView();
        List<Hall> hallList = hallsService.getAllHalls();
        modelAndView.setViewName("halls");
        modelAndView.addObject("halls_list", hallList);
        return modelAndView;
    }

    @PostMapping("/admin/panel/halls")
    public ModelAndView createHall(Hall hall){
        hallsService.addHall(hall);
        return new ModelAndView("redirect:/admin/panel/halls");
    }

}
