package ru.itdrive.cinema.repositories;

import ru.itdrive.cinema.models.Session;

public interface SessionRepository extends CrudRepository<Session>{
}
