package ru.itdrive.cinema.repositories;

import ru.itdrive.cinema.models.Hall;



public interface HallRepository extends CrudRepository<Hall> {

}
