package ru.itdrive.cinema.repositories;

import ru.itdrive.cinema.models.Hall;

import java.util.List;

public interface CrudRepository<T> {
    void save(T entity);
    T find(Long id);
    List<T> findAll();
}
