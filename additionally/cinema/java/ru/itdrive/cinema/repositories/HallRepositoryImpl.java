package ru.itdrive.cinema.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ru.itdrive.cinema.models.Hall;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class HallRepositoryImpl implements HallRepository {

    JdbcTemplate jdbcTemplate;

    @Autowired
    public HallRepositoryImpl(DataSource dataSource){
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    RowMapper<Hall> rowMapper = (row, i) -> Hall.builder()
            .id(row.getLong("id"))
            .countPlace(row.getInt("count_place"))
            .build();

    //language=SQL
    private static final String SQL_ADD_HALL = "insert into halls (count_place) values (?)";
    //language=SQL
    private static final String SQL_FIND_ALL = "select * from halls";
    //language=SQL
    private static final String SQL_FIND_HALL_BY_ID = "select * from halls where id = ?";

    @Override
    public void save(Hall entity) {
        jdbcTemplate.update(SQL_ADD_HALL,entity.getCountPlace());
    }

    @Override
    public Hall find(Long id) {
        return jdbcTemplate.queryForObject(SQL_FIND_HALL_BY_ID, rowMapper, id);
    }

    @Override
    public List<Hall> findAll() {
        return jdbcTemplate.query(SQL_FIND_ALL, rowMapper);
    }
}
