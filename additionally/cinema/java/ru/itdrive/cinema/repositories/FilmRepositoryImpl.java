package ru.itdrive.cinema.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.itdrive.cinema.models.Film;
import javax.sql.DataSource;
import java.util.List;

@Repository
public class FilmRepositoryImpl implements FilmRepository {

    JdbcTemplate jdbcTemplate;

    //language=SQL
    private static final String SQL_SAVE_FILM = "insert into films (title, release_year, age_rating, description, poster_url) VALUES (?,?,?,?,?)";

    //language=SQL
    private static final String SQL_FIND_ALL_FILM = "select * from films";

    //language=SQL
    private static final String SQL_FIND_FILM_BY_ID = "select * from films where id = ?";

    @Autowired
    public FilmRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    RowMapper<Film> rowMapper = (row, i) -> Film.builder()
            .id(row.getLong("id"))
            .title(row.getString("title"))
            .releaseYear(row.getInt("release_year"))
            .ageRating(row.getInt("age_rating"))
            .description(row.getString("description"))
            .posterUrl(row.getString("poster_url"))
            .build();

    @Override
    public void save(Film entity) {
        jdbcTemplate.update(SQL_SAVE_FILM, entity.getTitle(), entity.getReleaseYear(), entity.getAgeRating(), entity.getDescription(), entity.getPosterUrl());
    }

    @Override
    public Film find(Long id) {
        return jdbcTemplate.queryForObject(SQL_FIND_FILM_BY_ID, rowMapper, id);
    }

    @Override
    public List<Film> findAll() {
        return jdbcTemplate.query(SQL_FIND_ALL_FILM, rowMapper);
    }
}
