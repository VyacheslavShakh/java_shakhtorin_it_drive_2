package ru.itdrive.cinema.repositories;

import ru.itdrive.cinema.models.Film;

public interface FilmRepository extends CrudRepository<Film> {
}
