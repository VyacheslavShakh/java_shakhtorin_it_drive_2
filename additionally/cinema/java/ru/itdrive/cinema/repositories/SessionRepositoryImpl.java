package ru.itdrive.cinema.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.itdrive.cinema.models.Session;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class SessionRepositoryImpl implements SessionRepository {

    JdbcTemplate jdbcTemplate;

    //language=SQL
    private static final String SQL_ADD_SESSION = "insert into sessions(hall_id, film_id, session_time, ticket_cost) VALUES (?,?,?,?)";
    //language=SQL
    private static final String SQL_FIND_ALL_SESSION = "select * from sessions";
    //language=SQL
    private static final String SQL_FIND_SESSION_BY_ID = "select * from sessions";

    private RowMapper<Session> rowMapper = (row, i) -> Session.builder()
            .hallId(row.getLong("hall_id"))
            .filmId(row.getLong("film_id"))
            .sessionTime(row.getString("session_time"))
            .ticketCost(row.getFloat("ticket_cost"))
            .build();

    @Autowired
    SessionRepositoryImpl(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    @Override
    public void save(Session entity) {
        jdbcTemplate.update(SQL_ADD_SESSION, entity.getHallId(), entity.getFilmId(), entity.getSessionTime(), entity.getTicketCost());
    }

    @Override
    public Session find(Long id) {
        return jdbcTemplate.queryForObject(SQL_FIND_SESSION_BY_ID, rowMapper, id);
    }

    @Override
    public List<Session> findAll() {
        return jdbcTemplate.query(SQL_FIND_ALL_SESSION, rowMapper);
    }
}
