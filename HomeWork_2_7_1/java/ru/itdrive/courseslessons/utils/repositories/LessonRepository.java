package ru.itdrive.courseslessons.utils.repositories;

import ru.itdrive.courseslessons.utils.models.Lesson;

public interface LessonRepository extends CrudRepository<Lesson>{

}
