package ru.itdrive.courseslessons.utils.repositories;

import ru.itdrive.courseslessons.utils.models.Course;

public interface CourseRepository extends CrudRepository<Course> {
}
