package ru.itdrive.courseslessons.utils.repositories;

import ru.itdrive.courseslessons.utils.models.Course;
import ru.itdrive.courseslessons.utils.models.Lesson;

import java.sql.*;
import java.util.*;

public class CourseRepositoryImpl implements CourseRepository {

    Connection connection;
    //language=SQL
    private final String SQL_FIND_BY_ID = "select course.id as course_id, course.title, course.start_date, course.finish_date, lesson.id as lesson_id, lesson.name, lesson.id_course from course join lesson on course.id = lesson.id_course where course.id = ?";
    //language=SQL
    private final String SQL_FIND_ALL = "select course.id as course_id, course.title, course.start_date, course.finish_date, lesson.id as lesson_id, lesson.name, lesson.id_course from course join lesson on course.id = lesson.id_course";
    //language=SQL
    private final String SQL_SAVE = "insert into course(title, start_date, finish_date) value (?,?,?)";
    //language=SQL
    private final String SQL_UPDATE = "update course set title = ?, start_date = ?, finish_date = ? where id = ?";

    RowMapper<Course> courseRowMapper = new RowMapper<Course>() {
        public Course mapRow(ResultSet row) throws SQLException {
            return new Course(
                    row.getInt("course_id"),
                    row.getString("title"),
                    row.getTimestamp("start_date"),
                    row.getTimestamp("finish_date")
            );
        }
    };

    RowMapper<Lesson> lessonRowMapper = new RowMapper<Lesson>() {
        public Lesson mapRow(ResultSet row) throws SQLException {
            return new Lesson(
                    row.getInt("lesson_id"),
                    row.getString("name"),
                    row.getInt("id_course")
            );
        }
    };

    public List <Course> createMap(ResultSet resultSet) throws SQLException{
        Map <Integer, Course> courseMap = new HashMap<Integer, Course>();
        List <Course> courseList;
        int lastId = 0;
        int currentId;
        while (resultSet.next()){
            currentId = resultSet.getInt("course_id");
            if (lastId != currentId){
                courseMap.put(currentId, courseRowMapper.mapRow(resultSet));
                lastId = currentId;
            }
            Lesson lesson = lessonRowMapper.mapRow(resultSet);
            lesson.setCourse(courseMap.get(currentId));
            courseMap.get(currentId).getLessonList().add(lesson);
        }
        courseList = new ArrayList<Course>(courseMap.values());
        return courseList;
    }


    public CourseRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    public void save(Course object) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_SAVE);
            statement.setString(1, object.getTitle());
            statement.setTimestamp(2, object.getStart_date());
            statement.setTimestamp(3, object.getFinish_date());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    public void update(Course object) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, object.getTitle());
            statement.setTimestamp(2, object.getStart_date());
            statement.setTimestamp(3, object.getFinish_date());
            statement.setInt(4,object.getId());
            statement.executeUpdate();
            statement.close();

        }catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    public void delete(Integer id) {

    }

    public Course find(Integer id) {
        try {

            PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID);
            statement.setInt(1,id);
            ResultSet resultSet = statement.executeQuery();
            Course outCourse = createMap(resultSet).get(0);
            resultSet.close();
            statement.close();
            return outCourse;


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Course> findAll() {
        try {

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL);
            List<Course> courseList = createMap(resultSet);
            resultSet.close();
            statement.close();
            return courseList;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
