package ru.itdrive.courseslessons.utils.repositories;

import org.postgresql.core.SqlCommand;
import ru.itdrive.courseslessons.utils.models.Course;
import ru.itdrive.courseslessons.utils.models.Lesson;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LessonRepositoryImpl implements LessonRepository {

    private Connection connection;


    //language=SQL
    private final String SQL_FIND_BY_ID = "select course.id as course_id, course.title, course.start_date, course.finish_date, lesson.id as lesson_id, lesson.name, lesson.id_course from course join lesson on course.id = lesson.id_course where course.id in (select id_course from lesson where id = ?)";
    //language=SQL
    private final String SQL_FIND_ALL = "" +
            "select course.id as course_id, course.title, course.start_date, course.finish_date, lesson.id as lesson_id, lesson.name, lesson.id_course" +
            " from course join lesson on course.id = lesson.id_course";

    //language=SQL
    private final String SQL_SAVE = " ";

    RowMapper<Course> courseRowMapper = new RowMapper<Course>() {
        public Course mapRow(ResultSet row) throws SQLException {
            return new Course(
                    row.getInt("course_id"),
                    row.getString("title"),
                    row.getTimestamp("start_date"),
                    row.getTimestamp("finish_date")
            );
        }
    };

    RowMapper<Lesson> lessonRowMapper = new RowMapper<Lesson>() {
        public Lesson mapRow(ResultSet row) throws SQLException {
            return new Lesson(
                    row.getInt("lesson_id"),
                    row.getString("name"),
                    row.getInt("id_course")
            );
        }
    };

    public List<Lesson> createMap(ResultSet resultSet) throws SQLException {
        Map<Integer, Course> courseMap = new HashMap<Integer, Course>();

        List<Lesson> lessonList = new ArrayList<Lesson>();
        int lastId = 0;
        int currentId;
        while (resultSet.next()) {
            currentId = resultSet.getInt("course_id");
            if (lastId != currentId) {
                courseMap.put(currentId, courseRowMapper.mapRow(resultSet));
                lastId = currentId;
            }
            Lesson lesson = lessonRowMapper.mapRow(resultSet);
            lessonList.add(lesson);
            lesson.setCourse(courseMap.get(currentId));
            courseMap.get(currentId).getLessonList().add(lesson);
        }

        return lessonList;
    }


    public LessonRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    public void save(Lesson object) {
        try {
            PreparedStatement statement = connection.prepareStatement("insert into lesson (name, id_course) values (?,?)");
            statement.setString(1,object.getName());
            statement.setInt(2, object.getId_course());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void update(Lesson object) {
        try {
            PreparedStatement statement = connection.prepareStatement("update lesson set name = ?, id_course = ? where id = ?");
            statement.setString(1,object.getName());
            statement.setInt(2,object.getId_course());
            statement.setInt(3,object.getId());
            statement.executeUpdate();
            statement.close();
        }catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    public void delete(Integer id) {

    }

    public Lesson find(Integer id) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            Lesson outPutLesson = null;
            for (Lesson lesson: createMap(resultSet)){
                if (lesson.getId() == id){outPutLesson = lesson;}
            }
            resultSet.close();
            statement.close();
            return outPutLesson;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Lesson> findAll() {
        try {

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL);
            List<Lesson> lessonList = createMap(resultSet);
            resultSet.close();
            statement.close();
            return lessonList;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
