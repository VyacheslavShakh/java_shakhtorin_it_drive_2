package ru.itdrive.courseslessons.utils.models;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Course {
    private Integer id;
    private String title;
    private Timestamp start_date;
    private Timestamp finish_date;
    private List<Lesson>lessonList;


    public Timestamp getStart_date() {
        return start_date;
    }

    public Timestamp getFinish_date() {
        return finish_date;
    }

    public String getTitle() {
        return title;
    }

    public Course(Integer id, String title, Timestamp star_date, Timestamp finish_date) {
        this.id = id;
        this.title = title;
        this.start_date = star_date;
        this.finish_date = finish_date;
        lessonList = new ArrayList<Lesson>();
    }

    public Course(String title, Timestamp star_date, Timestamp finish_date) {
        this.title = title;
        this.start_date = star_date;
        this.finish_date = finish_date;
        lessonList = new ArrayList<Lesson>();
    }



    public Integer getId() {
        return id;
    }

    public List<Lesson> getLessonList() {
        return lessonList;
    }
}
