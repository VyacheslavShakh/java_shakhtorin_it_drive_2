package ru.itdrive.courseslessons.utils.models;

public class Lesson {

    private Integer id;
    private String name;
    private Integer id_course;
    private Course course;

    public Lesson(Integer id, String name, Integer id_course) {
        this.id = id;
        this.name = name;
        this.id_course = id_course;
    }
    public Lesson(String name, Integer id_course) {

        this.name = name;
        this.id_course = id_course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Course getCourse() {
        return course;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getId_course() {
        return id_course;
    }
}
