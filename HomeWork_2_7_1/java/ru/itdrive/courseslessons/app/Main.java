package ru.itdrive.courseslessons.app;


import ru.itdrive.courseslessons.utils.models.Lesson;
import ru.itdrive.courseslessons.utils.repositories.LessonRepositoryImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Main {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/education_center";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "030787";

    public static void main(String[] args) {
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            LessonRepositoryImpl lessonRepository = new LessonRepositoryImpl(connection);
            System.out.println(lessonRepository.find(2).getName() + " from " + lessonRepository.find(2).getCourse().getTitle());
            Lesson newLesson = new Lesson(2,"Astronomy",1);
//            lessonRepository.save(newLesson);
            lessonRepository.update(newLesson);
            System.out.println(lessonRepository.find(2).getName() + " from " + lessonRepository.find(2).getCourse().getTitle());

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }



    }
}
