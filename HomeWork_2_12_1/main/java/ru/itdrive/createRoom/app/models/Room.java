package ru.itdrive.createRoom.app.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Room {

    private Long id;
    private String roomName;
    private int maxCountUsersInRoom;

}
