package ru.itdrive.createRoom.app.servlets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.createRoom.app.models.Room;
import ru.itdrive.createRoom.app.repositories.RoomRepository;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/createRoom")
public class CreateRoomServlet extends HttpServlet {

    @Autowired
    RoomRepository roomRepository;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        roomRepository = context.getBean(RoomRepository.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("html/createRoom.html");
        requestDispatcher.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("roomName");
        String maxCountUsersAsString = request.getParameter("maxCountUser");
        int maxCountUsers = Integer.parseInt(maxCountUsersAsString);

        Room room = Room.builder().
                roomName(name).
                maxCountUsersInRoom(maxCountUsers).
                build();

        roomRepository.save(room);
        

    }
}
