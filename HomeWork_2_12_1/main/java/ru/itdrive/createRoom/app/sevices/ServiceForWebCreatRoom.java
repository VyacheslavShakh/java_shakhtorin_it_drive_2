package ru.itdrive.createRoom.app.sevices;

import org.springframework.beans.factory.annotation.Autowired;
import ru.itdrive.createRoom.app.models.Room;
import ru.itdrive.createRoom.app.repositories.RoomRepository;

public class ServiceForWebCreatRoom {

    @Autowired
    RoomRepository roomRepository;


    public void createRoom(Room room){
        roomRepository.save(room);
    }



}
