package ru.itdrive.createRoom.app.repositories;

import ru.itdrive.createRoom.app.models.Room;

public interface RoomRepository extends CrudRepository<Room> {
}
