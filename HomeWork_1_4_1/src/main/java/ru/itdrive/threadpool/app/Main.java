package ru.itdrive.threadpool.app;

import ru.itdrive.threadpool.util.ThreadPool;

public class Main {

    public static void main(String[] args) {
        ThreadPool threadPool = new ThreadPool(3);
        threadPool.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("A task execute in " + Thread.currentThread().getName());
            }
        });

        threadPool.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("B task execute in " + Thread.currentThread().getName());
            }
        });


        threadPool.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("C task execute in " + Thread.currentThread().getName());
            }
        });


    }


}
