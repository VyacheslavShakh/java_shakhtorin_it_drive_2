package ru.itdrive.threadpool.util;

import java.util.Deque;
import java.util.LinkedList;

public class ThreadPool {

    private PoolWorker threads[];

    private Deque<Runnable> tasks;

    public ThreadPool(int threadsCount) {
        this.tasks = new LinkedList<>();
        this.threads = new PoolWorker[threadsCount];
        for (int i = 0; i < this.threads.length; i++) {
            this.threads[i] = new PoolWorker();
            this.threads[i].start();
        }
    }

    public void submit(Runnable task) {
        synchronized (tasks) {
            tasks.add(task);
            tasks.notify();
        }
    }

    private class PoolWorker extends Thread {
        @Override
        public void run() {
            Runnable currentTask;

            while (true) {
                synchronized (tasks) {
                    while (tasks.peekFirst() == null) {
                        try {
                            tasks.wait(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    currentTask = tasks.pop();
                }
                    currentTask.run();

            }
        }
    }
}
