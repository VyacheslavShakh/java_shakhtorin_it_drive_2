package ru.itdrive.socketserver.utils.repositories;


import java.util.List;

public interface CrudRepository<T> {
    void save(T entity);
    T findByName(String name);
    List<T> findAllById(Long id);
    void update(T t);
    void delete(T t);
}
