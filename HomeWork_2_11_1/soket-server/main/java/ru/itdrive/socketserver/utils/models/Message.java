package ru.itdrive.socketserver.utils.models;


public class Message {
    private final User sender;
    private final Long room;
    private final String text;

    public Message(User sender, Long room, String text) {
        this.sender = sender;
        this.room = room;
        this.text = text;
    }

    public User getSender() {
        return sender;
    }

    public String getText() {
        return text;
    }

    public Long getRoom() {
        return room;
    }
}
