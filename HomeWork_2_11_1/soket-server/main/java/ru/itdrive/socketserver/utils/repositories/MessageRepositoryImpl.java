package ru.itdrive.socketserver.utils.repositories;

import org.springframework.stereotype.Component;
import ru.itdrive.socketserver.utils.models.Message;
import ru.itdrive.socketserver.utils.models.User;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Component
public class MessageRepositoryImpl implements MessageRepository {

    private DataSource dataSource;

    public MessageRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Message message) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("insert into all_messages (room_id, sender, text) values (?,?,?)");
            preparedStatement.setLong(1, message.getRoom());
            preparedStatement.setString(2, message.getSender().getNickName());
            preparedStatement.setString(3, message.getText());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    public List<Message> findAllById(Long room) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            List<Message> messageList = new ArrayList<>();
            preparedStatement = connection.prepareStatement("select * from (select * from all_messages where room_id = ? order by id desc limit 30 offset 0) as tab order by id ");
            preparedStatement.setLong(1, room);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                messageList.add(new Message(new User(resultSet.getString(3)),
                        room,
                        resultSet.getString(4)));
            }
            return messageList;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }


    @Override
    public Message findByName(String name) {
        return null;
    }

    @Override
    public void update(Message message) {

    }

    @Override
    public void delete(Message message) {

    }
}
