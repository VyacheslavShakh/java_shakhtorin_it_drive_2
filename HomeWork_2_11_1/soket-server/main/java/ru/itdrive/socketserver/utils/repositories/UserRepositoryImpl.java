package ru.itdrive.socketserver.utils.repositories;

import org.springframework.stereotype.Component;
import ru.itdrive.socketserver.utils.models.Room;
import ru.itdrive.socketserver.utils.models.User;
import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

@Component
public class UserRepositoryImpl implements UserRepository {

    DataSource dataSource;


    public UserRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    RowMapper<User> userRowMapper = resultSet -> new User(
            resultSet.getLong("id"),
            resultSet.getString("nickname"));


    @Override
    public void save(User user) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("insert into all_users (nickname) values (?);", 1);
            preparedStatement.setString(1, user.getNickName());
            int affectedRow = preparedStatement.executeUpdate();

            if (affectedRow != 1) {
                throw new SQLException("User not created");
            }

            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                user.setId(resultSet.getLong("id"));
            } else {
                throw new SQLException("Id not added to User");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public User findByName(String name) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("select * from all_users where nickname = ?");
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            if (!resultSet.isBeforeFirst()) return null;
            if (resultSet.next()) {
                User user = userRowMapper.mapRow(resultSet);
                preparedStatement = connection.prepareStatement("select * from rooms_user join rooms on rooms_user.room_id = rooms.id where user_id = ?");
                preparedStatement.setLong(1, user.getId());
                resultSet = preparedStatement.executeQuery();
                if (resultSet.isBeforeFirst()) {
                    resultSet.next();
                    Room currentRoom = new Room(resultSet.getLong("room_id"), resultSet.getString("title"));
                    user.setCurrentRoom(currentRoom);
                }
                return user;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public List<User> findAllById(Long id) {
        return null;
    }

    @Override
    public void update(User user) {

    }

    @Override
    public void delete(User user) {

    }
}
