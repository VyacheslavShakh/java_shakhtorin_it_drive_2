package ru.itdrive.socketserver.utils.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.socketserver.utils.models.Message;
import ru.itdrive.socketserver.utils.models.Room;
import ru.itdrive.socketserver.utils.models.User;
import ru.itdrive.socketserver.utils.repositories.*;
import java.util.List;

@Component
public class MessagesServiceImpl implements MessagesService {


    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private MessageRepository messageRepository;


//    public MessagesServiceImpl(UserRepository userRepository, RoomRepository roomRepository, MessageRepository messageRepository) {
//        this.userRepository = userRepository;
//        this.roomRepository = roomRepository;
//        this.messageRepository = messageRepository;
//    }

    public User addUserByName(String name) {
        User user;
        user = userRepository.findByName(name);
        if (user == null) {
            user = new User(name);
            userRepository.save(user);
        }
        return user;
    }

    public Room createRoom(String roomName) {
        Room room;
        room = roomRepository.findByName(roomName);
        if (room == null) {
            room = new Room(roomName);
            roomRepository.save(room);
        }
        return room;
    }

    public void insertUserAtRoom(Long roomId, Long userId) {
        roomRepository.addUserInRoom(roomId, userId);
    }

    public void exitUserFromRoom(Long roomId, Long userId){
        roomRepository.exitUserFromRoom(roomId, userId);
    }

    public void addMessage(Message message){
        messageRepository.save(message);
    }

    public List<Message> getLastMessages(Long roomId){
        return messageRepository.findAllById(roomId);
    }
 }
