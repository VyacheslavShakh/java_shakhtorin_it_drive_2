package ru.itdrive.socketserver.utils.models;



public class User{

    private Long id;
    private final String nickName;
    private Room currentRoom;

    public User(String nickName) {
        this.nickName = nickName;
        currentRoom = null;
    }

    public User(Long id, String nickName) {
        this.id = id;
        this.nickName = nickName;
        currentRoom = null;

    }

    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getNickName() {
        return nickName;
    }




}
