package ru.itdrive.socketserver.utils.services;

import ru.itdrive.socketserver.utils.models.Message;
import ru.itdrive.socketserver.utils.models.Room;
import ru.itdrive.socketserver.utils.models.User;

import java.util.List;

public interface MessagesService {


    User addUserByName(String name);

    Room createRoom(String roomName);

    void insertUserAtRoom(Long roomId, Long userId);

    void exitUserFromRoom(Long roomId, Long userId);

    void addMessage(Message message);

    List<Message> getLastMessages(Long roomId);
}
