package ru.itdrive.socketserver.utils.repositories;

import org.springframework.stereotype.Component;
import ru.itdrive.socketserver.utils.models.Room;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


@Component
public class RoomRepositoryImpl implements RoomRepository {

    DataSource dataSource;

    public RoomRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    RowMapper<Room> roomRowMapper = resultSet -> new Room(
            resultSet.getLong("id"),
            resultSet.getString("title"));

    @Override
    public void save(Room room) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("insert into rooms (title) values (?)", 1);
            preparedStatement.setString(1, room.getTitle());
            int affectidRows = preparedStatement.executeUpdate();

            if (affectidRows != 1) {
                throw new SQLException("Room not saved");
            }

            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                room.setId(resultSet.getLong("id"));
            } else {
                throw new SQLException("Id not added to Room");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    public void addUserInRoom(Long roomId, Long userId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("select * from rooms_user where room_id = ? and user_id = ?");
            preparedStatement.setLong(1, roomId);
            preparedStatement.setLong(2, userId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.isBeforeFirst()) return;
            preparedStatement = connection.prepareStatement("insert into rooms_user (room_id, user_id) VALUES (?,?)");
            preparedStatement.setLong(1, roomId);
            preparedStatement.setLong(2, userId);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    public void exitUserFromRoom(Long roomId, Long userId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("delete from rooms_user where room_id = ? and user_id = ?");
            preparedStatement.setLong(1, roomId);
            preparedStatement.setLong(2, userId);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public Room findByName(String name) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("select * from rooms where title = ?");
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            if (!resultSet.isBeforeFirst()) {
                return null;
            }
            if (resultSet.next()){
                return roomRowMapper.mapRow(resultSet);
            } else {
                return null;
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }

    }

    @Override
    public List<Room> findAllById(Long id) {
        return null;
    }

    @Override
    public void update(Room room) {

    }

    @Override
    public void delete(Room room) {

    }
}
