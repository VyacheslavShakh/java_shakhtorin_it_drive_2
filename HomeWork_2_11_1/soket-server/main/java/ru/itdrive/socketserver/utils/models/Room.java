package ru.itdrive.socketserver.utils.models;



import java.util.ArrayList;
import java.util.Deque;
import java.util.List;


public class Room {
    private Long id;
    private String title;
    private List<User> roomUsers;
    private Deque<Message> lastMessages;

    public Room(Long id, String title) {
        this.id = id;
        this.title = title;
        roomUsers = new ArrayList<>();
    }

    public Room(String title) {
        this.title = title;
        roomUsers = new ArrayList<>();
    }

    public void addUser (User user){
        roomUsers.add(user);
    }

    public List<User> getRoomUsers() {
        return roomUsers;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Deque<Message> getLastMessages() {
        return lastMessages;
    }

}