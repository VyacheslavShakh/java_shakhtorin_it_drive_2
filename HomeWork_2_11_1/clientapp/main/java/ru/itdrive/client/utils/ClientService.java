package ru.itdrive.client.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientService {

    Socket clientSocket;
    BufferedReader reader;
    PrintWriter writer;

    public ClientService(String host, int port) {
        try {
            clientSocket = new Socket(host, port);
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            writer = new PrintWriter(clientSocket.getOutputStream(), true);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        new Thread(listener).start();
    }

    public void sendMessage(String text) {
        writer.println(text);
    }

    public Runnable listener = new Runnable() {
        public void run() {

            while (true) {
                try {
                    String message = reader.readLine();
                    System.out.println(message);
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    };
}
