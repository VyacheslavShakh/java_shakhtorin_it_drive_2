package ru.itdrive.thread.app;


import com.beust.jcommander.JCommander;
import ru.itdrive.thread.utils.Arguments;
import ru.itdrive.thread.utils.FileResources;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        String path = arguments.getPaths();

        FileResources fileResources = new FileResources(path);

        List<String> keyList = new ArrayList<>(fileResources.getDirMap().keySet());

        for (int i = 0; i < keyList.size(); i++){
            System.out.println(keyList.get(i));
            fileResources.getDirMap().get(keyList.get(i));
            for (int j = 0; j < fileResources.getDirMap().get(keyList.get(i)).size(); j++){
                System.out.print("   " + fileResources.getDirMap().get(keyList.get(i)).get(j));
            }
            System.out.println();
        }

    }
}
