package ru.itdrive.thread.utils;

import java.io.File;
import java.util.*;

public class FileResources {

    String path;
    Map<String, List<String>> DirMap = new HashMap<>();

    public FileResources(String path) {
        File dir = new File(path);
        if (dir.isDirectory()) {
            for (File item : Objects.requireNonNull(dir.listFiles())) {
                Runnable task = () -> {
                    if (item.isDirectory()) {
                        List<String> FileInformation = new ArrayList<>();
                        for (File item2lvl : Objects.requireNonNull(item.listFiles())) {
                            FileInformation.add(item2lvl.getName());
                        }
                        DirMap.put(item.getName(), FileInformation);
                    }

                };
                task.run();
            }
        }

    }

    public Map<String, List<String>> getDirMap() {
        return DirMap;
    }
}
