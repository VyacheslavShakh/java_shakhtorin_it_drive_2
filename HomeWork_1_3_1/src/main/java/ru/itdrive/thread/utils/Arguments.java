package ru.itdrive.thread.utils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;


@Parameters (separators = "=")
public class Arguments {


    @Parameter (names = {"--path","-p"})
    String paths;


    public String getPaths() {
        return paths;
    }
}
