package ru.itdrive.webChat.app.sevices;

import ru.itdrive.webChat.app.models.FileInfo;
import ru.itdrive.webChat.app.models.Token;
import ru.itdrive.webChat.app.models.User;

import java.io.InputStream;
import java.util.List;

public interface ServiceForWebChat {

    User userSignUp (String userName, String password);

    Token userSignIn (String userName, String password);

    void upLoadFileOnServer(String uuidUser, InputStream inputStream, String fileName, String fileType, Long fileSize);

    List<FileInfo> getFileListByUser(String uuid);

    String getPathToFiles();

    Long getUserIdByUUID (String uuid);

    FileInfo getFileBuUuid (String uuid);


}
