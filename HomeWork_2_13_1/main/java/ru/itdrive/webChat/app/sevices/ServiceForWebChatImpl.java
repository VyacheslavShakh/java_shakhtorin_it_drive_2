package ru.itdrive.webChat.app.sevices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.webChat.app.models.FileInfo;
import ru.itdrive.webChat.app.models.Token;
import ru.itdrive.webChat.app.models.User;
import ru.itdrive.webChat.app.repositories.FileRepository;
import ru.itdrive.webChat.app.repositories.RoomRepository;
import ru.itdrive.webChat.app.repositories.TokenRepository;
import ru.itdrive.webChat.app.repositories.UserRepository;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;


@Component
public class ServiceForWebChatImpl implements ServiceForWebChat {

    @Autowired
    RoomRepository roomRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    TokenRepository tokenRepository;
    @Autowired
    FileRepository fileRepository;

    public final String PATHTOFILEFOLDER = "D://Java/serverfiles/";


    @Override
    public User userSignUp(String userName, String password) {
        User user = userRepository.findUserByName(userName);
        if (user == null) {
            user = User.builder().name(userName).password(password).build();
            userRepository.save(user);
        } else {
            user = null;
        }

        return user;
    }

    @Override
    public Token userSignIn(String userName, String password) {
        User user = userRepository.findUserByName(userName);
        Token token = Token.builder().currentUser(user).build();

        if (user != null) {
            if (user.getPassword().substring(0,password.length()).equals(password)) {
                token.createUuid();
                tokenRepository.save(token);
            } else {
                token.setUuid(null);
            }
        } else {
            token.setCurrentUser(null);
        }
        return token;
    }

    @Override
    public void upLoadFileOnServer(String uuidUser, InputStream inputStream, String fileName, String fileType, Long fileSize){
        Long idUser  = tokenRepository.findUserBuUUID(uuidUser);
        FileInfo fileInfo = FileInfo.builder().
                userId(idUser).
                fileName(fileName).
                uuidFile(UUID.randomUUID().toString()).
                mimeType(fileType).
                fileSize(fileSize).
                build();
        fileRepository.save(fileInfo);
        String[] wordsInFileName = fileName.split("\\.");

        try {
            Files.copy(inputStream, Paths.get(PATHTOFILEFOLDER + fileInfo.getUuidFile()+ "." + wordsInFileName[wordsInFileName.length-1]));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public List<FileInfo> getFileListByUser(String uuid){
        Long currentUserId = tokenRepository.findUserBuUUID(uuid);
        return fileRepository.findAllFilesByUserId(currentUserId);
    }

    @Override
    public Long getUserIdByUUID (String uuid){
        return tokenRepository.findUserBuUUID(uuid);
    }

    @Override
    public String getPathToFiles() {
        return PATHTOFILEFOLDER;
    }

    @Override
    public FileInfo getFileBuUuid (String uuid){
        return fileRepository.findBuUuid(uuid);
    }

}
