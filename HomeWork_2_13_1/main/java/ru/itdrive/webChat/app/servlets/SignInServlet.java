package ru.itdrive.webChat.app.servlets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.webChat.app.models.Token;
import ru.itdrive.webChat.app.sevices.ServiceForWebChat;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {


    ServiceForWebChat serviceForWebChat;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springConfig");
        serviceForWebChat = applicationContext.getBean(ServiceForWebChat.class);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("userName");
        String password = request.getParameter("password");
        Token token = serviceForWebChat.userSignIn(name, password);
        if (token.getCurrentUser() == null) {
            PrintWriter pw = response.getWriter();
            pw.println("<html>");
            pw.println("<p>User with this name not exist <a href = /signUp >Register now</a></p>");
            pw.println("</html>");
        } else if (token.getUuid() == null) {
            PrintWriter pw = response.getWriter();
            pw.println("<html>");
            pw.println("<p>Wrong password <a href = /signIn >Try again</a></p>");
            pw.println("</html>");
        } else {
            Cookie cookie = new Cookie("uuid", token.getUuid());
            response.addCookie(cookie);
            response.sendRedirect("/files");
        }

    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("html/signIn.html").forward(request, response);
    }
}
