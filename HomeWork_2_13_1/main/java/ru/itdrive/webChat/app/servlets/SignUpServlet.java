package ru.itdrive.webChat.app.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.webChat.app.models.User;
import ru.itdrive.webChat.app.sevices.ServiceForWebChat;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/signUp")
public class SignUpServlet extends HttpServlet {

    ServiceForWebChat serviceForWebChat;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springConfig");
        serviceForWebChat = applicationContext.getBean(ServiceForWebChat.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("html/signUp.html").forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("userName");
        String password = request.getParameter("passwordFirst");
        if (password.equals(request.getParameter("passwordSecond"))){
            User user = serviceForWebChat.userSignUp(userName, password);
            if (user != null){
                request.getRequestDispatcher("html/signIn.html").forward(request,response);
            } else  {
                PrintWriter pw = response.getWriter();
                pw.println("<html>");
                pw.println("<p>User with this name already exist <a href = /signIn >Try again!</a></p>");
                pw.println("</html>");
            }
        } else {
            PrintWriter pw = response.getWriter();
            pw.println("<html>");
            pw.println("<p>Password mismatch <a href = /signUp >Try again!</a></p>");
            pw.println("</html>");
        }
    }


}
