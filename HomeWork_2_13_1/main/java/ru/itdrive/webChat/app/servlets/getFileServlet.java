package ru.itdrive.webChat.app.servlets;

import org.springframework.context.ApplicationContext;
import ru.itdrive.webChat.app.models.FileInfo;
import ru.itdrive.webChat.app.sevices.ServiceForWebChat;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;


@WebServlet("/file/*")
@MultipartConfig
public class getFileServlet extends HttpServlet {

    ServiceForWebChat serviceForWebChat;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springConfig");
        serviceForWebChat = applicationContext.getBean(ServiceForWebChat.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] requestUri = request.getRequestURI().split("/");
        String fileUuid = requestUri[requestUri.length-1];
        FileInfo fileInfo = serviceForWebChat.getFileBuUuid(fileUuid);
        String fullFileName = fileInfo.getUuidFile().substring(0, fileInfo.getUuidFile().indexOf(" ")) + "." + fileInfo.getFileName().split("\\.")[fileInfo.getFileName().split("\\.").length-1].substring(0,fileInfo.getFileName().split("\\.")[fileInfo.getFileName().split("\\.").length-1].indexOf(" "));
        File currentFile = new File(serviceForWebChat.getPathToFiles()+fullFileName);
        response.setContentType(fileInfo.getMimeType());
        response.setContentLength((int)(long)fileInfo.getFileSize());
        response.setHeader();
        Files.copy(currentFile.toPath(), response.getOutputStream());

    }
}
