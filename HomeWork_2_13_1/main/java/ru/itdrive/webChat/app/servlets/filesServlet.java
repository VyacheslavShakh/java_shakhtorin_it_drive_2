package ru.itdrive.webChat.app.servlets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.webChat.app.sevices.ServiceForWebChat;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.InputStream;


@WebServlet("/files")
@MultipartConfig
public class filesServlet extends HttpServlet {


    ServiceForWebChat serviceForWebChat;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springConfig");
        serviceForWebChat = applicationContext.getBean(ServiceForWebChat.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("html/files.html").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        String currentUserUuid = null;
        for (Cookie cookie: cookies){
            if(cookie.getName().equals("uuid")){
                currentUserUuid = cookie.getValue();
            }
        }
        Part part = request.getPart("file");
        InputStream fileStream = part.getInputStream();
        String fileName = part.getSubmittedFileName();
        String fileType = part.getContentType();
        Long fileSize = part.getSize();
        serviceForWebChat.upLoadFileOnServer(currentUserUuid, fileStream, fileName, fileType, fileSize);
        response.sendRedirect("/files");
    }


}
