package ru.itdrive.webChat.app.servlets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.webChat.app.models.FileInfo;
import ru.itdrive.webChat.app.sevices.ServiceForWebChat;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/filelist")
public class FileListServlet extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springConfig");
        serviceForWebChat = applicationContext.getBean(ServiceForWebChat.class);
    }


    ServiceForWebChat serviceForWebChat;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter printWriter = response.getWriter();
        Cookie[] cookies = request.getCookies();
        printWriter.println("<html>");
        for (Cookie cookie : cookies){
            if(cookie.getName().equals("uuid")){
                printWriter.println("<p>Your file list</p>");


                for (FileInfo fileInfo : serviceForWebChat.getFileListByUser(cookie.getValue())){

                    printWriter.println("<p><a href = /file/"+ fileInfo.getUuidFile() +">"+fileInfo.getFileName()+"</a></p>");
                }
            }
        }
        printWriter.println("</html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
