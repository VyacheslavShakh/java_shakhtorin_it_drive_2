package ru.itdrive.webChat.app.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Token {

    User currentUser;
    String uuid;

    public void createUuid(){
        this.uuid = UUID.randomUUID().toString();
    }

}
