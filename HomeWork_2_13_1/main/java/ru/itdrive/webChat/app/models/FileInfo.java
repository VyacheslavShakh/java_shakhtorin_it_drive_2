package ru.itdrive.webChat.app.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FileInfo {

    Long fileId;
    Long userId;
    String fileName;
    String mimeType;
    Long fileSize;
    String uuidFile;

}
