package ru.itdrive.webChat.app.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.webChat.app.models.FileInfo;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@Component
public class FileRepositoryImpl implements FileRepository {

    @Autowired
    DataSource dataSource;

    Function<ResultSet, FileInfo> rowMapper = resultSet -> {
        FileInfo fileInfo;
        try {
            fileInfo = FileInfo.builder().
                    fileId(resultSet.getLong("id")).
                    userId(resultSet.getLong("user_id")).
                    fileName(resultSet.getString("file_name")).
                    mimeType(resultSet.getString("mime_type")).
                    fileSize(resultSet.getLong("file_size")).
                    uuidFile(resultSet.getString("uuid")).
                    build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return fileInfo;
    };

    @Override
    public void save(FileInfo entity) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("insert into files (user_id, file_name, mime_type, file_size, uuid) values (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, entity.getUserId());
            preparedStatement.setString(2, entity.getFileName());
            preparedStatement.setString(3, entity.getMimeType());
            preparedStatement.setLong(4, entity.getFileSize());
            preparedStatement.setString(5, entity.getUuidFile());
            preparedStatement.executeUpdate();
            entity.setFileId((long) preparedStatement.RETURN_GENERATED_KEYS);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }

    }

    @Override
    public List<FileInfo> findAllFilesByUserId(Long userId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<FileInfo> fileList = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("select * from files where user_id = ?");
            preparedStatement.setLong(1, userId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                fileList.add(rowMapper.apply(resultSet));
            }
            return fileList;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public FileInfo findBuUuid(String uuid) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        FileInfo fileInfo = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("select * from files where uuid = ?");
            preparedStatement.setString(1, uuid);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                fileInfo = rowMapper.apply(resultSet);
            }
            return fileInfo;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public void update(FileInfo entity) {

    }

    @Override
    public FileInfo findById(Long id) {
        return null;
    }

    @Override
    public List<FileInfo> findAll() {
        return null;
    }
}
