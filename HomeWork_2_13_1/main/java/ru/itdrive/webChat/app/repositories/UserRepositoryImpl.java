package ru.itdrive.webChat.app.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.webChat.app.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.function.Function;

@Component
public class UserRepositoryImpl implements UserRepository {


    Function<ResultSet, User> rowMapper = resultSet -> {
        User user;
        try {
            user = User.builder().
                    id(resultSet.getLong("id")).
                    name(resultSet.getString("name")).
                    password(resultSet.getString("password")).
                    build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
            return user;
    };


    @Autowired
    DataSource dataSource;

    @Override
    public void save(User entity) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("insert into users_for_web (name, password) values (?, ?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1,entity.getName());
            preparedStatement.setString(2,entity.getPassword());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()){
                entity.setId(resultSet.getLong("id"));
            } else {
                throw new SQLException("User was not created");
            }
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null){
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public void update(User entity) {

    }

    @Override
    public User findById(Long id) {
        return null;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User findUserByName(String name){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User currentUser;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("select * from users_for_web where name = ?");
            preparedStatement.setString(1 ,name);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                currentUser = rowMapper.apply(resultSet);
            } else {
                currentUser = null;
            }

            return currentUser;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null){
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }

    }
}
