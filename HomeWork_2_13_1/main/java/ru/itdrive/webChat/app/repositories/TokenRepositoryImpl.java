package ru.itdrive.webChat.app.repositories;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.webChat.app.models.Token;
import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

@Component
public class TokenRepositoryImpl implements TokenRepository{


    @Autowired
    DataSource dataSource;

    @Override
    public void save(Token entity) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            System.out.println("user saved");
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("delete from tokens where user_id = ?");
            preparedStatement.setLong(1, entity.getCurrentUser().getId());
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement("insert into tokens (uuid, user_id) VALUES (?,?)");
            preparedStatement.setString(1, entity.getUuid());
            preparedStatement.setLong(2,entity.getCurrentUser().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }  if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }

        }
    }

    @Override
    public void update(Token entity) {

    }

    @Override
    public Token findById(Long id) {
        return null;
    }

    @Override
    public List<Token> findAll() {
        return null;
    }

    @Override
    public Long findUserBuUUID(String uuid) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Long userId;
        try {
            System.out.println("Enter in query");
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("select * from tokens where uuid = ?");
            preparedStatement.setString(1, uuid);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                userId = resultSet.getLong("user_id");
                System.out.println("User Id" + userId);
            } else {
                userId = null;
            }
            return userId;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }finally {
            if (resultSet != null){
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }


    }
}
