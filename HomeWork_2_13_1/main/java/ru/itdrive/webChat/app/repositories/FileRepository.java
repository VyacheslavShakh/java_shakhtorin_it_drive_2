package ru.itdrive.webChat.app.repositories;

import ru.itdrive.webChat.app.models.FileInfo;

import java.util.List;


public interface FileRepository extends CrudRepository<FileInfo> {

    List<FileInfo> findAllFilesByUserId(Long userId);

    FileInfo findBuUuid(String uuid);

}
