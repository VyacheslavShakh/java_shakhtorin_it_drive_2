package ru.itdrive.webChat.app.repositories;

import ru.itdrive.webChat.app.models.User;

public interface UserRepository extends CrudRepository<User> {
    User findUserByName(String name);
}
