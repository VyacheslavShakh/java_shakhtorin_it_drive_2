package ru.itdrive.webChat.app.repositories;

import java.util.List;

public interface CrudRepository <T> {

    void save(T entity);
    void update(T entity);
    T findById(Long id);
    List<T> findAll();

}
