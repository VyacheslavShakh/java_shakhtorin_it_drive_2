package ru.itdrive.webChat.app.repositories;

import ru.itdrive.webChat.app.models.Token;

public interface TokenRepository extends CrudRepository<Token> {

    Long findUserBuUUID(String uuid);

}
