package ru.itdrive.webChat.app.repositories;

import ru.itdrive.webChat.app.models.Room;

public interface RoomRepository extends CrudRepository<Room> {
}
