package ru.itdrive.webChat.app.repositories;

import ru.itdrive.webChat.app.models.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User> {
    Optional<User> findUserByName(String name);
}
