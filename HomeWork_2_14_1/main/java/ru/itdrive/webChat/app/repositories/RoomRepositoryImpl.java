package ru.itdrive.webChat.app.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.webChat.app.models.Room;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Component
public class RoomRepositoryImpl implements RoomRepository{

    @Autowired
    DataSource dataSource;

    //language=SQL
    public final String SQL_SAVE_ROOM = "insert into rooms_for_web (name, max_count_user) values (?,?)";

    @Override
    public void save(Room entity) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SAVE_ROOM, statement.RETURN_GENERATED_KEYS);
            statement.setString(1, entity.getRoomName());
            statement.setInt(2,entity.getMaxCountUsersInRoom());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()){
                entity.setId(resultSet.getLong("id"));
            } else throw new SQLException("Room does not created");

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet == null){
                try {
                    resultSet.close();
                } catch (SQLException ignored) {}
            }
            if(statement == null){
                try {
                    statement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection == null){
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }

    }

    @Override
    public void update(Room entity) {

    }

    @Override
    public Optional<Room> findById(Long id) {
        return null;
    }

    @Override
    public List<Room> findAll() {
        return null;
    }
}
