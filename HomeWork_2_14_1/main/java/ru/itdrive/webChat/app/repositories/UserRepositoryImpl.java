package ru.itdrive.webChat.app.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.webChat.app.models.User;
import ru.itdrive.webChat.app.models.UserRole;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Component
public class UserRepositoryImpl implements UserRepository {

    //language=SQL
    public final String SQL_SAVE_USER = "insert into users_for_web (name, password, role) values (? ,?, ?)";
    //language=SQL
    public final String FIND_USER_BY_ID = "select * from users_for_web where id = ?";
    //language=SQL
    public final String FIND_USER_BY_NAME = "select * from users_for_web where name = ?";



    Function<ResultSet, User> rowMapper = resultSet -> {
        User user;
        try {
            user = User.builder().
                    id(resultSet.getLong("id")).
                    name(resultSet.getString("name")).
                    password(resultSet.getString("password")).
                    userRole(UserRole.valueOf(resultSet.getString("role"))).
                    build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
            return user;
    };

    @Autowired
    DataSource dataSource;

    @Override
    public void save(User entity) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(SQL_SAVE_USER, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1,entity.getName());
            preparedStatement.setString(2,entity.getPassword());
            preparedStatement.setString(3, entity.getUserRole().name());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()){
                entity.setId(resultSet.getLong("id"));
            } else {
                throw new SQLException("User was not created");
            }
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null){
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public void update(User entity) {

    }

    @Override
    public Optional<User> findById(Long id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User currentUser;
        try{
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(FIND_USER_BY_ID);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                currentUser = rowMapper.apply(resultSet);
                return Optional.of(currentUser);
            }
            return Optional.empty();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null){
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    @Override
    public List<User> findAll() {

        return null;
    }

    @Override
    public Optional<User> findUserByName(String name){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(FIND_USER_BY_NAME);
            preparedStatement.setString(1 ,name);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                return Optional.of(rowMapper.apply(resultSet));
            }
            return Optional.empty();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null){
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
            if (preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException ignored) {
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }

    }
}
