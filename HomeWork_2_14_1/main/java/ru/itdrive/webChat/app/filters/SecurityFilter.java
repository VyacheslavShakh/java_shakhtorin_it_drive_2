package ru.itdrive.webChat.app.filters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.itdrive.config.AppProperties;
import ru.itdrive.config.ApplicationConfig;
import ru.itdrive.webChat.app.models.Authenication;
import ru.itdrive.webChat.app.models.UserRole;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Component
@WebFilter("/*")
public class SecurityFilter implements Filter {

    AppProperties appProperties;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (appProperties == null){
            ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
            appProperties = applicationContext.getBean(AppProperties.class);
        }
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        System.out.println("In Security Filter with request " + request.getRequestURI());
        if (!isProtected(request.getRequestURI())) {
            filterChain.doFilter(servletRequest, servletResponse);

        } else {
            HttpSession session = request.getSession(false);
            if (session != null) {
                Authenication authenication = (Authenication) session.getAttribute("authentication");
                if (authenication != null && authenication.getAuthenticated()) {
                    UserRole userRole = authenication.getUser().getUserRole();
                    if ((isAccessAccept(userRole, request.getRequestURI()))||request.getRequestURI().startsWith("/file")) {
                        filterChain.doFilter(servletRequest,servletResponse);
                        return;
                    }
                }
            }
            ResponseUtil.sendForbidden(request,response);
        }
    }

    private boolean isProtected(String path) {
        return !path.startsWith("html/errorPage.html") && !path.startsWith("/home") && !path.startsWith("/signUp") && !path.startsWith("/signIn") && !path.startsWith("/favicon.ico");
    }

    private boolean isAccessAccept(UserRole userRole, String url) {
        List<String> acceptUrls;
        switch (userRole.name().toLowerCase()) {
            case ("user"):
                acceptUrls = appProperties.getAcceptedRolesForUser();
                break;
            case ("admin"):
                acceptUrls = appProperties.getAcceptedRolesForAdmin();
                break;
            default:
                return false;
        }
        for (String urls:acceptUrls) {
            if (urls.equals(url)){
                return true;
            }
        }
        return false;
    }

    @Override
    public void destroy() {

    }
}
