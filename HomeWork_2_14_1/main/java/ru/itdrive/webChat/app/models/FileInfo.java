package ru.itdrive.webChat.app.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FileInfo {

    private Long fileId;
    private Long userId;
    private String fileName;
    private String mimeType;
    private Long fileSize;
    private String uuidFile;

}
