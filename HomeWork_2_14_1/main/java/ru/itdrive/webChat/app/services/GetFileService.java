package ru.itdrive.webChat.app.services;

import ru.itdrive.webChat.app.models.FileInfo;

public interface GetFileService {

    FileInfo getFileByUuid(String fileUuid);

    String getPathToFiles();
}
