package ru.itdrive.webChat.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.config.AppProperties;
import ru.itdrive.webChat.app.models.FileInfo;
import ru.itdrive.webChat.app.repositories.FileRepository;

import java.util.List;

@Component
public class FileListServiceImpl implements FileListService {

    @Autowired
    FileRepository fileRepository;
    @Autowired
    AppProperties appProperties;


    @Override
    public List<FileInfo> getFileListByUser(Long userId){
        return fileRepository.findAllFilesByUserId(userId);
    }

    @Override
    public String getPatToServerFileFolders(){
        return appProperties.getPathToFileFolderOnServer();
    }



}
