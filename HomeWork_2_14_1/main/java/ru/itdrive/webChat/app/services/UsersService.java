package ru.itdrive.webChat.app.services;

import ru.itdrive.webChat.app.models.User;

import java.util.Optional;

public interface UsersService {

    Optional<User> findUserById(Long id);

}
