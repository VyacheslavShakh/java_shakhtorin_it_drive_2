package ru.itdrive.webChat.app.services;

import ru.itdrive.webChat.app.models.Authenication;
import ru.itdrive.webChat.app.models.User;

public interface SignInService {

    User userSignIn(String userName);

    Authenication authenticate (String name, String password);

}
