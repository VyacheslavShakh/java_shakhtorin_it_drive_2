package ru.itdrive.webChat.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itdrive.webChat.app.models.Authenication;
import ru.itdrive.webChat.app.models.User;
import ru.itdrive.webChat.app.repositories.UserRepository;

import java.util.Optional;


@Component
public class SignInServiceImpl implements SignInService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User userSignIn(String userName) {
        return userRepository.findUserByName(userName).orElse(new User());
    }

    @Override
    public Authenication authenticate(String userName, String password) {
        Optional<User> optionalUser = userRepository.findUserByName(userName);
        if (optionalUser.isPresent()) {
            return Authenication.builder().user(optionalUser.get()).authenticated(true).build();
        }
        return null;
    }
}
