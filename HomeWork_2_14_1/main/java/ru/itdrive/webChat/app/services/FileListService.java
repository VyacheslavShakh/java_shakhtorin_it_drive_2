package ru.itdrive.webChat.app.services;

import ru.itdrive.webChat.app.models.FileInfo;

import java.util.List;

public interface FileListService {

    List<FileInfo> getFileListByUser(Long userId);

    String getPatToServerFileFolders();

}
