package ru.itdrive.webChat.app.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/logout")
public class LogOutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("enter in logout");
        HttpSession session = request.getSession(false);
        session.invalidate();
        request.getRequestDispatcher("html/logout.html").forward(request,response);
    }

}
