package ru.itdrive.webChat.app.servlets;

import org.springframework.context.ApplicationContext;
import ru.itdrive.webChat.app.models.Authenication;
import ru.itdrive.webChat.app.services.UpLoadFileService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.InputStream;


@WebServlet("/fileupload")
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {


    UpLoadFileService service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springConfig");
        service = applicationContext.getBean(UpLoadFileService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("html/fileupload.html").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        Authenication authenication = (Authenication) session.getAttribute("authentication");
        Part part = request.getPart("file");
        InputStream fileStream = part.getInputStream();
        String fileName = part.getSubmittedFileName();
        String fileType = part.getContentType();
        Long fileSize = part.getSize();
        service.upLoadFileOnServer(authenication.getUser().getId(), fileStream, fileName, fileType, fileSize);
        response.sendRedirect("/fileupload");
    }


}
