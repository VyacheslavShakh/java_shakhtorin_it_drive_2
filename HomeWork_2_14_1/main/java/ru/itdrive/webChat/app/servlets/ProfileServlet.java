package ru.itdrive.webChat.app.servlets;

import ru.itdrive.webChat.app.models.Authenication;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null){
            Authenication authenication = (Authenication) session.getAttribute("authentication") ;
            request.setAttribute("user", authenication.getUser());
            request.getRequestDispatcher("jsp/profilePage.jsp").forward(request,response);
        }
    }

}
