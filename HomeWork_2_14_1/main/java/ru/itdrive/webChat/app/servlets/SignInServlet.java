package ru.itdrive.webChat.app.servlets;

import org.springframework.context.ApplicationContext;
import ru.itdrive.webChat.app.models.Authenication;
import ru.itdrive.webChat.app.services.SignInService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;


@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {

    SignInService service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springConfig");
        service = applicationContext.getBean(SignInService.class);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("userName");
        String password = request.getParameter("password");
        Authenication authenication = service.authenticate(name, password);
        if (authenication.getAuthenticated()) {
            HttpSession session = request.getSession();
            session.setAttribute("authentication", authenication);


            String pathForRedirect = request.getParameter("redirect");
            if (pathForRedirect != null) {
                response.sendRedirect(pathForRedirect);
            } else {
                response.sendRedirect("/profile");
            }
        } else {
            response.setStatus(403);
            request.getRequestDispatcher("html/errorPage.html").forward(request, response);
        }

    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("html/signIn.html").forward(request, response);
    }
}
