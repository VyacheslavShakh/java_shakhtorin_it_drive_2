<%@ page import="ru.itdrive.webChat.app.models.FileInfo" %>
<%@ page import="ru.itdrive.webChat.app.models.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>File list</title>
</head>
<body>
<h1>List of files</h1>
<% Map<String, String> filePathsMap = (Map<String, String>)request.getAttribute("filePathsMap");
    for (Map.Entry <String, String> file: filePathsMap.entrySet()){%>
    <p><a href="/file/<%=file.getKey()%>"> <%=file.getValue()%></a></p>
<%}%>
</body>
</html>
