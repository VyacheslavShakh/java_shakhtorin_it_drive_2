<%@ page import="ru.itdrive.webChat.app.models.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
</head>
<body>
<h1>Hello, ${user.getName()}</h1>
<p>You can:</p>
<p><a href = "/fileupload">Upload yor files</a></p>
<p>Work with your <a href="/filelist">files</a></p>
<p><a href="/logout">Exit from profile</a></p>
</body>
</html>
