package ru.itdrive.socketserver.utils;


import java.util.ArrayList;
import java.util.List;

public class User{


    private Integer id;
    private final String nickName;
    Room currentRoom;
    private ClientThread clientThread;

    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }

    public ClientThread getClientThread() {
        return clientThread;
    }

    public void setClientThread(ClientThread clientThread) {
        this.clientThread = clientThread;
    }

    public User(String nickName) {

        this.nickName = nickName;

    }

    public User(Integer id, String nickName) {
        this.id = id;
        this.nickName = nickName;

    }


    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getNickName() {
        return nickName;
    }




}
