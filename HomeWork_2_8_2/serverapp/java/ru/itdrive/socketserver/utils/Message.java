package ru.itdrive.socketserver.utils;

public class Message {
    private final User sender;
    private final Room room;
    private final String text;

    public Message(User sender, Room room, String text) {
        this.sender = sender;
        this.room = room;
        this.text = text;
    }

    public void sendMessage(){
        for (User user: room.getRoomUsers()){
            if (!user.equals(sender)){
                System.out.println();
                user.getClientThread().send(sender.getNickName() + ":  " + text);
            }
        }

    }

    public User getSender() {
        return sender;
    }

    public Room getRoom() {
        return room;
    }

    public String getText() {
        return text;
    }
}
