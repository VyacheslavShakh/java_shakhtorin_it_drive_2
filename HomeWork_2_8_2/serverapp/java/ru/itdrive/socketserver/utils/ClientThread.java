package ru.itdrive.socketserver.utils;

import java.io.*;
import java.net.Socket;
import java.sql.Connection;

import ru.itdrive.socketserver.app.Main;
import ru.itdrive.socketserver.utils.repositories.RoomRepositoryImpl;
import ru.itdrive.socketserver.utils.repositories.UserRepositoryImpl;

public class ClientThread extends Thread {

    private Socket clientSocket;
    Server server;
    InputStream inputStream;
    BufferedReader clientReader;
    PrintWriter out;
    public User user;
    UserRepositoryImpl userRepository;
    RoomRepositoryImpl roomRepository;
    public Room currentRoom = null;


    public ClientThread(Socket clientSocket, Server server, UserRepositoryImpl userRepository, RoomRepositoryImpl roomRepository) {
        this.clientSocket = clientSocket;
        this.server = server;
        this.userRepository = userRepository;
        this.roomRepository = roomRepository;
        start();
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    @Override
    public void run() {
        try {
            inputStream = clientSocket.getInputStream();
            clientReader = new BufferedReader(new InputStreamReader(inputStream));
            out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);
            String inputLine;
            this.send("Enter your nickname");
            inputLine = clientReader.readLine();
            user = userRepository.read(inputLine);
            if (user == null) {
                user = userRepository.create(inputLine);
            }
            user.setClientThread(this);

            this.send("You successful enter as " + user.getNickName());


            while (true) {


                inputLine = clientReader.readLine();


                if (inputLine.startsWith("choose room ")) {
                    String roomName = inputLine.substring(12);

                    if (server.serversRoom.containsKey(roomName)) {
                        currentRoom = server.serversRoom.get(roomName);
                        roomRepository.addUserInRoom(currentRoom, user);
                        this.send("You enter in room: " + currentRoom.getTitle());
                        for (Message lastMessage : currentRoom.getLastMessages()) {
                            this.send(lastMessage.getSender().getNickName() + ": " + lastMessage.getText());
                        }
                        continue;
                    }

                    currentRoom = roomRepository.read(roomName);
                    if (currentRoom == null) {
                        currentRoom = roomRepository.create(roomName);

                    }

                    server.serversRoom.put(roomName, currentRoom);
                    roomRepository.addUserInRoom(currentRoom, user);


                    this.send("You enter in room: " + currentRoom.getTitle());
                    continue;

                }

                if (inputLine.equals("exit from room")) {
                    user.setCurrentRoom(null);
                    roomRepository.exitUserFromRepository(currentRoom, user);
                    server.serversRoom.remove(user.getNickName());
                    user.setCurrentRoom(null);
                    continue;
                }


                if (currentRoom == null) {
                    continue;
                }


                for (User user : currentRoom.getRoomUsers()) {
                    System.out.println("в комнате пользователь" + user.getNickName());
                }

                Message currentMessage = new Message(user, currentRoom, inputLine);

                currentRoom.newMessage(currentMessage);

                currentMessage.sendMessage();

            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


    }

    public void send(String text) {
        out.println(text);
    }
}
