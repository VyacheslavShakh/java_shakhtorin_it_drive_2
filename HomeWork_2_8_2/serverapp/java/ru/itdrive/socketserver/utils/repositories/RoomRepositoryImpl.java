package ru.itdrive.socketserver.utils.repositories;

import ru.itdrive.socketserver.utils.Room;
import ru.itdrive.socketserver.utils.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RoomRepositoryImpl implements RoomRepository {

    Connection connection;
    UserRepositoryImpl userRepository;

    public RoomRepositoryImpl(Connection connection){
        this.connection = connection;
    }

    RowMapper<Room> roomRowMapper = new RowMapper<Room>() {
        @Override
        public Room mapRow(ResultSet resultSet) throws SQLException {
            return new Room(
                    resultSet.getInt("id"),
                    resultSet.getString("title"));
        }
    };

    @Override
    public Room create(String nickname) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into rooms (title) values (?)" , 1);
            preparedStatement.setString(1,nickname);
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            Room room = new Room(resultSet.getInt("id"),nickname);

            return room;

        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    public void addUserInRoom (Room room, User user){
        try {

            room.addUser(user);

            user.setCurrentRoom(room);

            PreparedStatement preparedStatement = connection.prepareStatement("select * from rooms_user where room_id = ? and user_id = ?");
            preparedStatement.setInt(1, room.getId());
            preparedStatement.setInt(2, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.isBeforeFirst()) return;
            preparedStatement = connection.prepareStatement("insert into rooms_user (room_id, user_id) VALUES (?,?)");
            preparedStatement.setInt(1,room.getId());
            preparedStatement.setInt(2,user.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    public void exitUserFromRepository(Room room, User user){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from rooms_user where room_id = ? and user_id = ?");
            preparedStatement.setInt(1,room.getId());
            preparedStatement.setInt(2,user.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Room read(String nickname) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from rooms where title = ?");
            preparedStatement.setString(1,nickname);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.isBeforeFirst()){
                return null;
            }
            resultSet.next();
            Room room = roomRowMapper.mapRow(resultSet);



            return room;
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void update(Room room) {

    }

    @Override
    public void delete(Room room) {

    }
}
