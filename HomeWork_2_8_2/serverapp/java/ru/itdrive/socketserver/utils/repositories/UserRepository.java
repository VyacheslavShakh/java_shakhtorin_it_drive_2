package ru.itdrive.socketserver.utils.repositories;

import ru.itdrive.socketserver.utils.User;

public interface UserRepository extends CrudRepository<User> {
}
