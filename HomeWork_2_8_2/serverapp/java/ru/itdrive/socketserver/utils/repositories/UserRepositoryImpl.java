package ru.itdrive.socketserver.utils.repositories;

import ru.itdrive.socketserver.utils.Room;
import ru.itdrive.socketserver.utils.User;

import java.sql.*;

public class UserRepositoryImpl implements UserRepository {

    Connection connection;


    public UserRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    RowMapper<User> userRowMapper = new RowMapper<User>() {
        @Override
        public User mapRow(ResultSet resultSet) throws SQLException {

            return new User(
                    resultSet.getInt("id"),
                    resultSet.getString("nickname"));

        }
    };


    @Override
    public User create(String nickName) {
        try {
            PreparedStatement statement = connection.prepareStatement("insert into all_users (nickname) values (?);", 1);
            statement.setString(1, nickName);
            statement.executeUpdate();
            ResultSet returnedId = statement.getGeneratedKeys();
            returnedId.next();
            User user = new User(nickName);
            user.setId(returnedId.getInt("id"));
            returnedId.close();
            statement.close();
            return user;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User read(String nickname) {
        try {
            PreparedStatement statement = connection.prepareStatement("select * from all_users where nickname = ?");
            statement.setString(1, nickname);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) return null;
            resultSet.next();
            User user = userRowMapper.mapRow(resultSet);
            statement = connection.prepareStatement("select all_users.nickname, rooms_user.room_id, rooms_user.user_id, rooms.title from all_users join rooms_user on all_users.id = rooms_user.user_id join rooms on rooms_user.room_id = rooms.id where rooms_user.room_id in (select rooms_user.room_id from rooms_user where rooms_user.user_id = ?)");
            statement.setInt(1, user.getId());
            resultSet = statement.executeQuery();
            int currentId;
            int lastId = 0;
            while (resultSet.next()) {

                if (!resultSet.isBeforeFirst()){break;}


                currentId = resultSet.getInt("room_id");
                if (lastId != currentId){
                    user.setCurrentRoom(new Room(resultSet.getInt("room_id"), resultSet.getString("title")));
                    lastId = currentId;
                }

                if (resultSet.getInt("user_id") != user.getId()) {
                    user.getCurrentRoom().addUser(new User(resultSet.getInt("user_id"), resultSet.getString("nickname")));
                }
            }
            resultSet.close();
            statement.close();
            return user;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User user) {

    }

    @Override
    public void delete(User user) {

    }
}
