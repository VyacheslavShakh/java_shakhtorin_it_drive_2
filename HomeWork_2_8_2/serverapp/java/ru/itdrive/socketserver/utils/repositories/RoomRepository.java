package ru.itdrive.socketserver.utils.repositories;

import ru.itdrive.socketserver.utils.Room;

public interface RoomRepository extends CrudRepository<Room> {
}
