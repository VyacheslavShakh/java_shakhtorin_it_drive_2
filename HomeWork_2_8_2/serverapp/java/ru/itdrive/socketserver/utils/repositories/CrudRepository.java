package ru.itdrive.socketserver.utils.repositories;

public interface CrudRepository<T> {
    T create(String nickname);
    T read (String nickname);
    void update(T t);
    void delete(T t);
}
