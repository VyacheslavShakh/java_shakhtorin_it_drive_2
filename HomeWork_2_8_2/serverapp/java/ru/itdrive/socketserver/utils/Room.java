package ru.itdrive.socketserver.utils;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class Room {
    private Integer id;
    private String title;
    private List<User> roomUsers;
    private Deque<Message> lastMessages;

    public Room(Integer id, String title) {
        this.id = id;
        this.title = title;
        roomUsers = new ArrayList<>();
        lastMessages = new ArrayDeque<>();
    }

    public Room(String title) {
        this.title = title;
        roomUsers = new ArrayList<>();
        lastMessages = new ArrayDeque<>(30);

    }

    public void addUser (User user){
        roomUsers.add(user);
    }

    public void newMessage(Message message){
        lastMessages.addLast(message);
        if (lastMessages.size()>30){
            lastMessages.removeFirst();
        }
    }

    public List<User> getRoomUsers() {
        return roomUsers;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Deque<Message> getLastMessages() {
        return lastMessages;
    }

}