package ru.itdrive.socketserver.utils;

import ru.itdrive.socketserver.utils.repositories.RoomRepositoryImpl;
import ru.itdrive.socketserver.utils.repositories.UserRepositoryImpl;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Server {

    public List <ClientThread> clientList = new ArrayList<>();
    public UserRepositoryImpl userRepository;
    public RoomRepositoryImpl roomRepository;
    public Map<String, Room> serversRoom;

    public Map<String, Room> getServersRoom() {
        return serversRoom;
    }

    public Room setRoomBiTitle (Room room){
        String roomTitle = room.getTitle();
        if (!serversRoom.containsKey(roomTitle)){
            serversRoom.put(roomTitle, room);
        }
        return serversRoom.get(roomTitle);
    }

    public void start (int port){

        final String DB_URL = "jdbc:postgresql://localhost:5432/base_for_chat";
        final String DB_USER = "postgres";
        final String DB_PASSWORD = "030787";

        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            userRepository = new UserRepositoryImpl(connection);
            roomRepository = new RoomRepositoryImpl(connection);
            serversRoom = new HashMap<>();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        Socket currentSocketClient;
        int i = 0;
        while (true){
            try {
                currentSocketClient = serverSocket.accept();
                clientList.add(i,new ClientThread(currentSocketClient, this, userRepository, roomRepository));
                i++;
            } catch (IOException e){
                throw new IllegalArgumentException(e);
            }

        }

    }

    public List<ClientThread> getClientList() {
        return clientList;
    }
}
