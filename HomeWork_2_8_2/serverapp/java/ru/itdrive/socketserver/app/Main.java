package ru.itdrive.socketserver.app;

import com.beust.jcommander.JCommander;
import ru.itdrive.socketserver.utils.Server;

public class Main {


    public static void main(String[] args) {

        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);


        Server server = new Server();
        server.start(arguments.port);

    }

}
