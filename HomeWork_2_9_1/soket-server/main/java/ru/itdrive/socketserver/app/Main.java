package ru.itdrive.socketserver.app;

import com.beust.jcommander.JCommander;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itdrive.socketserver.utils.SocketServer;
import ru.itdrive.socketserver.utils.services.MessagesService;

public class Main {


    public static void main(String[] args) {

        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");


        SocketServer server = new SocketServer(context.getBean(MessagesService.class));
        server.startServer(arguments.port);



    }

}
