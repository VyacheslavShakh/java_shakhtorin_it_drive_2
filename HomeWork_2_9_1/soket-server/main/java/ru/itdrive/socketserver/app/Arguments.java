package ru.itdrive.socketserver.app;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.Parameter;


@Parameters (separators = "=")
public class Arguments {

    @Parameter (names = {"--port"})
    int port;
}
