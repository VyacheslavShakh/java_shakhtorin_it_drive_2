package ru.itdrive.socketserver.utils.repositories;

import java.util.List;

public interface CrudRepository<T> {
    T save(String nickname);
    T find(String nickname);
    void update(T t);
    void delete(T t);
}
