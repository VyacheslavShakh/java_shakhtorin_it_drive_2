package ru.itdrive.socketserver.utils.repositories;

import ru.itdrive.socketserver.utils.models.User;

public interface UserRepository extends CrudRepository<User> {
}
