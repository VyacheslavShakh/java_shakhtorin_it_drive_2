package ru.itdrive.socketserver.utils.jdbc;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

public class DataSourceSimpleImpl implements DataSource {

    private String url;
    private String userName;
    private String password;
    private String driverClassName;

    private Connection connection;

    public DataSourceSimpleImpl(String url, String userName, String password, String driverClassName) {
        this.url = url;
        this.userName = userName;
        this.password = password;
        this.driverClassName = driverClassName;
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()){
            try {
                Class.forName(driverClassName);
                connection = DriverManager.getConnection(url,userName,password);
            } catch (ClassNotFoundException e){
                throw new IllegalArgumentException(e);
            }
        }
        return connection;
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {

    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {

    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return 0;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }
}

