package ru.itdrive.socketserver.utils.repositories;

import ru.itdrive.socketserver.utils.models.Message;
import ru.itdrive.socketserver.utils.models.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MessageRepositoryImpl implements MessageRepository {

    private Connection connection;

    public MessageRepositoryImpl(DataSource dataSource){
        try {
            this.connection = dataSource.getConnection();

        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }


    public void addMessage (Message message){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into all_messages (room_id, sender, text) values (?,?,?)");
            preparedStatement.setLong(1,message.getRoom());
            preparedStatement.setString(2, message.getSender().getNickName());
            preparedStatement.setString(3, message.getText());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    public List<Message> getMessageList (Long room){
        try {
            List <Message> messageList = new ArrayList<>();
            PreparedStatement preparedStatement = connection.prepareStatement("select * from (select * from all_messages where room_id = ? order by id desc limit 30 offset 0) as tab order by id ");
            preparedStatement.setLong(1, room);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                messageList.add(new Message(new User(resultSet.getString(3)),
                        room,
                        resultSet.getString(4)));
            }
            resultSet.close();
            preparedStatement.close();
            return messageList;
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }



    @Override
    public Message save(String nickname) {
        return null;
    }

    @Override
    public Message find(String nickname) {
        return null;
    }

    @Override
    public void update(Message message) {

    }

    @Override
    public void delete(Message message) {

    }
}
