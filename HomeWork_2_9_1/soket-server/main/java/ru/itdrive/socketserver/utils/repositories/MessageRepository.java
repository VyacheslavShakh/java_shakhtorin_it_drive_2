package ru.itdrive.socketserver.utils.repositories;

import ru.itdrive.socketserver.utils.models.Message;

import java.util.List;


public interface MessageRepository extends CrudRepository<Message> {

    void addMessage (Message message);

    List<Message> getMessageList (Long room);

}
