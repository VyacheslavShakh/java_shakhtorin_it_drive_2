package ru.itdrive.socketserver.utils.repositories;

import ru.itdrive.socketserver.utils.models.Room;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RoomRepositoryImpl implements RoomRepository {

    Connection connection;

    public RoomRepositoryImpl(DataSource dataSource){
        try {
            this.connection = dataSource.getConnection();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    RowMapper<Room> roomRowMapper = new RowMapper<Room>() {
        @Override
        public Room mapRow(ResultSet resultSet) throws SQLException {
            return new Room(
                    resultSet.getLong("id"),
                    resultSet.getString("title"));
        }
    };

    @Override
    public Room save(String nickname) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into rooms (title) values (?)" , 1);
            preparedStatement.setString(1,nickname);
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            Room room = new Room(resultSet.getLong("id"),nickname);

            return room;

        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    public void addUserInRoom (Long roomId, Long userId){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from rooms_user where room_id = ? and user_id = ?");
            preparedStatement.setLong(1, roomId);
            preparedStatement.setLong(2, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.isBeforeFirst()) return;
            preparedStatement = connection.prepareStatement("insert into rooms_user (room_id, user_id) VALUES (?,?)");
            preparedStatement.setLong(1,roomId);
            preparedStatement.setLong(2,userId);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    public void exitUserFromRoom(Long roomId, Long userId){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from rooms_user where room_id = ? and user_id = ?");
            preparedStatement.setLong(1,roomId);
            preparedStatement.setLong(2,userId);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Room find(String nickname) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from rooms where title = ?");
            preparedStatement.setString(1,nickname);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.isBeforeFirst()){
                return null;
            }
            resultSet.next();
            Room room = roomRowMapper.mapRow(resultSet);



            return room;
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void update(Room room) {

    }

    @Override
    public void delete(Room room) {

    }
}
