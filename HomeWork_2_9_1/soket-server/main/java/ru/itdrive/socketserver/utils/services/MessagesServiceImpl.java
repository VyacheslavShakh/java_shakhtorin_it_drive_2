package ru.itdrive.socketserver.utils.services;


import ru.itdrive.socketserver.utils.models.Message;
import ru.itdrive.socketserver.utils.models.Room;
import ru.itdrive.socketserver.utils.models.User;
import ru.itdrive.socketserver.utils.repositories.*;
import java.util.List;

public class MessagesServiceImpl implements MessagesService {


    private UserRepository userRepository;
    private RoomRepository roomRepository;
    private MessageRepository messageRepository;


    public MessagesServiceImpl(UserRepository userRepository, RoomRepository roomRepository, MessageRepository messageRepository) {
        this.userRepository = userRepository;
        this.roomRepository = roomRepository;
        this.messageRepository = messageRepository;
    }

    public User addUserByName(String name) {
        User user;
        user = userRepository.find(name);
        if (user == null) {
            user = userRepository.save(name);
        }
        return user;
    }

    public Room createRoom(String roomName) {
        Room room;
        room = roomRepository.find(roomName);
        if (room == null) {
            room = roomRepository.save(roomName);
        }
        return room;
    }

    public void insertUserAtRoom(Long roomId, Long userId) {
        roomRepository.addUserInRoom(roomId, userId);
    }

    public void exitUserFromRoom(Long roomId, Long userId){
        roomRepository.exitUserFromRoom(roomId, userId);
    }

    public void addMessage(Message message){
        messageRepository.addMessage(message);
    }

    public List<Message> getLastMessages(Long roomId){
        return messageRepository.getMessageList(roomId);
    }
 }
