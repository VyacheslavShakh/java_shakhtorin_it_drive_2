package ru.itdrive.socketserver.utils.repositories;

import ru.itdrive.socketserver.utils.models.Room;
import ru.itdrive.socketserver.utils.models.User;

import javax.sql.DataSource;
import java.sql.*;

public class UserRepositoryImpl implements UserRepository {

    Connection connection;


    public UserRepositoryImpl(DataSource dataSource) {
        try {
            this.connection = dataSource.getConnection();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    RowMapper<User> userRowMapper = resultSet -> new User(
            resultSet.getLong("id"),
            resultSet.getString("nickname"));


    @Override
    public User save(String nickName) {
        try {
            PreparedStatement statement = connection.prepareStatement("insert into all_users (nickname) values (?);", 1);
            statement.setString(1, nickName);
            statement.executeUpdate();
            ResultSet returnedId = statement.getGeneratedKeys();
            returnedId.next();
            User user = new User(nickName);
            user.setId(returnedId.getLong("id"));
            returnedId.close();
            statement.close();
            return user;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User find(String nickname) {
        try {
            PreparedStatement statement = connection.prepareStatement("select * from all_users where nickname = ?");
            statement.setString(1, nickname);
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.isBeforeFirst()) return null;
            resultSet.next();
            User user = userRowMapper.mapRow(resultSet);
            statement = connection.prepareStatement("select * from rooms_user join rooms on rooms_user.room_id = rooms.id where user_id = ?");
            statement.setLong(1, user.getId());
            resultSet = statement.executeQuery();
            if (resultSet.isBeforeFirst()) {
                resultSet.next();
                Room currentRoom = new Room(resultSet.getLong("room_id"), resultSet.getString("title"));
                user.setCurrentRoom(currentRoom);
            }
            resultSet.close();
            statement.close();
            return user;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User user) {

    }

    @Override
    public void delete(User user) {

    }
}
