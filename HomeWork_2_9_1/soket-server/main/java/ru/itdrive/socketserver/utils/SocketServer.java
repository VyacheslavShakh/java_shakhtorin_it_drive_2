package ru.itdrive.socketserver.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itdrive.socketserver.utils.models.Message;
import ru.itdrive.socketserver.utils.models.Room;
import ru.itdrive.socketserver.utils.models.User;
import ru.itdrive.socketserver.utils.services.MessagesService;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SocketServer {


    private MessagesService messagesService;
    private List<ChatSocketClient> anonymous;
    private Map<Long, List<ChatSocketClient>> users;


    private List<String> userNames;
    private Map<String, Room> rooms;

    private ServerSocket serverSocket;

    public SocketServer(MessagesService messagesService) {

        this.messagesService = messagesService;
        this.users = new HashMap<>();
        this.anonymous = new ArrayList<>();
        this.userNames = new ArrayList<>();
        this.rooms = new HashMap<>();

    }

    public void startServer(int port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        Socket currentSocketClient;

        while (true) {
            try {
                currentSocketClient = serverSocket.accept();
                anonymous.add(new ChatSocketClient(currentSocketClient));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    private class ChatSocketClient extends Thread {
        private Socket clientSocket;
        private InputStream inputStream;
        private BufferedReader clientReader;
        private PrintWriter out;
        private Long user;
        private Long room = null;
        private User currentUser;


        public ChatSocketClient(Socket clientSocket) {
            this.clientSocket = clientSocket;
            try {
                this.inputStream = clientSocket.getInputStream();
                this.clientReader = new BufferedReader(new InputStreamReader(inputStream));
                this.out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
            start();
        }

        @Override
        public void run() {

            String inputLine;
            signUp();

            while (true) {

                if (this.room == null) {
                    sendMessage("you can enter in chat room (choose room 'name room')");
                }

                inputLine = getMessage();

                if (inputLine.startsWith("choose room")) {

                    String roomName = inputLine.substring(12);

                    this.room = enterInRoom(roomName).getId();

                    continue;
                }

                if (inputLine.equals("exit from room")) {

                    exitUserFromRoom();

                    room = null;

                }
                if (room == null) {
                    continue;
                }

                Message currentMessage = new Message(currentUser, room, inputLine);
                this.sendMessageInRoom(currentMessage);
                messagesService.addMessage(currentMessage);

            }
        }

        public Long getUser() {
            return user;
        }

        public void setUser(Long user) {
            this.user = user;
        }

        public void signUp() {
            String inputLine;
            User user;
            while (true) {
                sendMessage("Enter your nickname");
                inputLine = getMessage();
                if (userNames.contains(inputLine)) {
                    sendMessage("User with this name exists, try another one");
                    continue;
                }
                sendMessage("You want enter as " + inputLine + "(enter yes to accept)");
                if (getMessage().equals("yes")) {
                    user = messagesService.addUserByName(inputLine);
                    sendMessage("You enter as " + inputLine);
                    this.setUser(user.getId());
                    if (user.getCurrentRoom() != null) {
                        if (!users.containsKey(user.getCurrentRoom().getId())) {
                            users.put(user.getCurrentRoom().getId(), new ArrayList<>());
                        }
                        users.get(user.getCurrentRoom().getId()).add(this);
                        sendMessage("You enter in chat room " + user.getCurrentRoom().getTitle());
                        this.room = user.getCurrentRoom().getId();

                        for (Message message : messagesService.getLastMessages(room)) {
                            sendMessage(message.getSender().getNickName() + ":" + message.getText());
                        }


                    }
                    this.currentUser = user;
                    return;
                }
            }
        }


        private void sendMessage(String text) {
            out.println(text);
        }

        private void sendMessageInRoom(Message message) {
            for (ChatSocketClient client : users.get(room)) {
                if (!this.user.equals(client.user)) {
                    client.sendMessage(message.getSender().getNickName() + ":" + message.getText());
                }
            }
        }

        private String getMessage() {
            try {
                return clientReader.readLine();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }


        public Room enterInRoom(String roomName) {
            Room room = messagesService.createRoom(roomName);
            if (!users.containsKey(room.getId())) {
                users.put(room.getId(), new ArrayList<>());
            }
            users.get(room.getId()).add(this);
            messagesService.insertUserAtRoom(room.getId(), user);
            sendMessage("You enter in chat room " + room.getTitle());
            this.room = room.getId();
            for (Message message : messagesService.getLastMessages(this.room)) {
                sendMessage(message.getSender().getNickName() + ":" + message.getText());
            }
            return room;
        }


        public void exitUserFromRoom() {
            if (this.room == null) {
                sendMessage("you not in any chat room");
                return;
            }
            messagesService.exitUserFromRoom(room, user);
        }


    }

}
