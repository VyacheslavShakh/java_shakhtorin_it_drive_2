package ru.itdrive.socketserver.utils.repositories;

import ru.itdrive.socketserver.utils.models.Room;

public interface RoomRepository extends CrudRepository<Room> {

    void addUserInRoom (Long roomId, Long userId);

    void exitUserFromRoom(Long roomId, Long userId);
}
