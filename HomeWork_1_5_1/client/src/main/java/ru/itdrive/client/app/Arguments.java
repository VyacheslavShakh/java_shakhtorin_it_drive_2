package ru.itdrive.client.app;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.Parameter;

@Parameters (separators = "=")
public class Arguments {

    @Parameter (names = {"--host"})
    String host;

    @Parameter (names = {"--port"})
    int port;

}
