package ru.itdrive.client.app;

import com.beust.jcommander.JCommander;
import ru.itdrive.client.utils.ClientService;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);



        ClientService clientService = new ClientService(arguments.host, arguments.port );
        Scanner scanner = new Scanner(System.in);

        while (true){
            String message = scanner.nextLine();
            clientService.sendMessage(message);
        }
    }
}
