package ru.itdrive.socketserver.utils;

import java.io.*;
import java.net.Socket;
import ru.itdrive.socketserver.app.Main;

public class ClientThread extends Thread {

    private Socket clientSocket;
    private String name;
    InputStream inputStream;
    BufferedReader clientReader;
    PrintWriter out;


    public ClientThread(Socket clientSocket) {
        this.clientSocket = clientSocket;
        start();
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    @Override
    public void run() {
        try {
            inputStream = clientSocket.getInputStream();
            clientReader = new BufferedReader(new InputStreamReader(inputStream));
            out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);
            String inputLine;
            while (true){
                inputLine = clientReader.readLine();
                for (ClientThread ct : Server.clientList){
                    ct.send(inputLine);

                }

            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }



    }

    public void send (String text) {
        out.println(text);
    }
}
