package ru.itdrive.socketserver.utils;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

    public static List <ClientThread> clientList = new ArrayList<>();;


    public void start (int port){


        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        Socket currentSocketClient;
        int i = 0;
        while (true){
            try {
                currentSocketClient = serverSocket.accept();
                clientList.add(i,new ClientThread(currentSocketClient));
                i++;
            } catch (IOException e){
                throw new IllegalArgumentException(e);
            }

        }

    }

}
