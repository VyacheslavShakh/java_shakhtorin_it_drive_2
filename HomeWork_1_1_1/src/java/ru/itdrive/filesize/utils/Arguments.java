package ru.itdrive.filesize.utils;

import com.beust.jcommander.Parameters;
import com.beust.jcommander.Parameter;

@Parameters(separators = " ")
public class Arguments {
	@Parameter(names = {"path"})
	public String path;
}


