package ru.itdrive.filesize.app;

import com.beust.jcommander.JCommander;
import ru.itdrive.FileList;
import ru.itdrive.filesize.utils.Arguments;

public class Main {

	public static void main(String[] args) {
		
	Arguments arguments = new Arguments();

	JCommander.newBuilder()
	          .addObject(arguments)
	          .build()
	          .parse(args);
    
    FileList fl = new FileList(arguments.path);

    fl.printList();


    }

    



}